package main;

import java.io.File;

import javax.xml.bind.JAXBException;
//import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import xml.scheme.dom.DOMParser;
import xml.scheme.dom.DOMWriter;
import xml.scheme.test.Marshalling;
import xml.scheme.test.Unmarshalling;

public class Main {

	public static void main(String[] args) throws TransformerFactoryConfigurationError, TransformerException, ParserConfigurationException, JAXBException {
		File f = new File("obavestenje.xml");
		File f2 = new File("zalbacutanjecir.xml");
		File f3 = new File("zalbanaodlukucir.xml");
		File f4 = new File("resenja(marshalovana).xml");
		/*
		 * Za jaxB, trenutno ne radi
		 */
		//Unmarshalling unmarshalling = new Unmarshalling();
		//unmarshalling.unmarshallObavestenje(f);
		
		/*
		 * za DOM, parsiara dokument "f" i ispisuje ga na ekran, trebalo bi da radi za
		 * proizvoljni dokument
		 */
		System.out.println("MARSHALOVANJE -> KREIRANJE RESENJA.XML");
		Marshalling marshalling = new Marshalling();
		marshalling.Marshall("resenja(marshalovana).xml");
		System.out.println("UNMARSHALOVANJE -> CITANJE RESENJA.XML");
		Unmarshalling unmarshalling = new Unmarshalling();
		unmarshalling.unmarshallResenja(f4);
		
		DOMParser dp = new DOMParser();
		dp.buildDocument(f);
		dp.printElement();
		System.out.println("===============================================");
		System.out.println();
		dp.buildDocument(f2);
		dp.printElement();
		System.out.println("===============================================");
		System.out.println();
		dp.buildDocument(f3);
		dp.printElement();
		
		
		
		/*
		 * pravi xml dokument "output.xml", treba da je isti kao "obavestenje.xml"
		 */
		DOMWriter dw = new DOMWriter();
		dw.createDocument();
		dw.generateDOM();
		dw.saveDocumentAsFile("output.xml");
		
		DOMWriter dw2 = new DOMWriter();
		dw2.createDocument();
		dw2.generateDOMZalbaCutanje();
		dw2.saveDocumentAsFile("output2.xml");
		
		DOMWriter dw3 = new DOMWriter();
		dw3.createDocument();
		dw3.generateDOMZalbaOdluka();
		dw3.saveDocumentAsFile("outputZalbaNaOdluku.xml");
	}

}
