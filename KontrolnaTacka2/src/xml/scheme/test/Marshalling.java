package xml.scheme.test;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import xml.scheme.model.Resenja;
import xml.scheme.model.TOsoba;
import xml.scheme.model.Resenja.Resenje;
import xml.scheme.model.Resenja.Resenje.TekstObrazlozenja;
import xml.scheme.model.Resenja.Resenje.TekstResenja;
import xml.scheme.model.Resenja.Resenje.Uvod;

public class Marshalling {
	
	private JAXBContext context;
	private Marshaller marshaller;
	
	public Marshalling() throws JAXBException {
		super();
		context = JAXBContext.newInstance("xml.scheme.model");
		marshaller = context.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	}
	
	public void Marshall(String filename) throws JAXBException {
		
		Resenja resenja = new Resenja();
		
		resenja.setResenje(createResenje());
		
		marshaller.marshal(resenja, new File(filename));
		
	}

	private Resenje createResenje() {
		
		Resenje resenje = new Resenje();
		resenje.setBroj("071-01-1114/2020-03");
		resenje.setTip("Kad je zalba odbijena");
		resenje.setDatum("09.06.2020. godine.");
		resenje.setUvod(createUvod());
		resenje.setTekstResenja(createTekstResenja());
		resenje.setTekstObrazlozenja(createTekstObrazlozenja());
		
		TOsoba tosoba = new TOsoba();
		tosoba.setIme("Miki");
		tosoba.setPrezime("Manojlovic");
		resenje.setPoverenik(tosoba);
		
		return resenje;
	}


	private Uvod createUvod() {
	
		Uvod uvod = new Uvod();
		TOsoba tosoba = new TOsoba();
		tosoba.setIme("Pera");
		tosoba.setPrezime("Peric");
		
		uvod.setPodnosilacZalbe(tosoba);
		
		uvod.setRazlogZalbe("Zalba u vezi sa....");
		uvod.setDatumZahteva("12.12.2012");
		uvod.setZakon("Na osnovu clana 5. zakona o....");
		return uvod;
	}
	
	private TekstResenja createTekstResenja() {
		
		TekstResenja tekstResenja = new TekstResenja();
		tekstResenja.setPrimalacResenja("Visi sud u Kraljevu....");
		
		return tekstResenja;
	}
	
	private TekstObrazlozenja createTekstObrazlozenja() {
		
		TekstObrazlozenja tekstObrazlozenja = new TekstObrazlozenja();
		tekstObrazlozenja.setRazlogZalbe("Zalba zbog...");
		tekstObrazlozenja.setPostupanjePoZalbi("Naredjeno je da se...");
		tekstObrazlozenja.setStaJeUtvrdjeno("Utvrdjeno je da se...");
		tekstObrazlozenja.setStaJeNaredjeno("Naredjeno je da se ....");
		tekstObrazlozenja.setUputstvoOPravnomSredstvu("Da se sto pre dostavi...");
		
		return tekstObrazlozenja;
	}
	

}
