import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogData } from '../user-create-appeal/user-create-appeal.component';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { XonomyService } from 'src/app/core/services/xonomy/xonomy.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { DecisionService } from 'src/app/core/services/decisions-service/decision.service';
import { NotificationService } from 'src/app/core/services/notification.service';
declare const Xonomy: any;

@Component({
  selector: 'app-dialog-insert-appeal-informations',
  templateUrl: './dialog-insert-appeal-informations.component.html',
  styleUrls: ['./dialog-insert-appeal-informations.component.css']
})



export class DialogInsertAppealInformationsComponent implements OnInit {
  isAdmin: boolean;
  ime: string;
  prezime: string;
  email: string;
  podaci: any;
  brojZalbe: string;
  seasons: string[] = ['nije postupio', 'nije postupio u celosti', 'u zakonskom roku'];
  zalba_zbog: string;
  setuj: boolean;

  setovano_rjesenje: boolean;
  opcije: string[] = ['zalba se odbija iz formalnih razloga', 'zalba se odbija jer je neosnovana', 'organu vlasti se nalaže da traženu informaciju dostavi tražiocu'];
  rjesenje_opcija: string;
  unijetiPodaci: boolean = false;

  constructor(
    private authService: AuthenticationService,
    private xonomyService: XonomyService,
    public dialogRef: MatDialogRef<DialogInsertAppealInformationsComponent>,
    private serviceDecision: DecisionService,
    private notificationService: NotificationService,

    @Inject(MAT_DIALOG_DATA) public data: DialogData) { }

  ngOnInit(): void {

    this.setuj = true;
    this.setovano_rjesenje = false;
    this.ime = this.authService.getCurrentUser().fullName.split(' ')[0];
    this.prezime = this.authService.getCurrentUser().fullName.split(' ')[1];
    this.email = this.authService.getCurrentUser().email;

    this.isAdmin = this.authService.getCurrentUser().isAdmin;
    if (!this.isAdmin) {
      if (this.data.isDecision) {
        this.setuj = false;
        this.brojZalbe = 'RJ-' + this.data.id.slice(4, 5) + '-555';
        this.insertDataDecisionXonomy();
      } else {
        let tip = document.getElementById("select");
      }
    } else {
      console.log('hehe');
      let opcija_rjesenje = document.getElementById("opcija_rjesenje_id");

    }

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  insertDataDecisionXonomy() {
    let element = document.getElementById("showXonomy");
    let xmlString =
      `<zalba xmlns="http://www.euprava.poverenik.gov.rs/appealDecision" xmlns:pred="http://www.euprava.poverenik.gov.rs/predicate/">
    <vrsta_zalbe>ЖАЛБА ПРОТИВ ОДЛУКЕ ОРГАНА ВЛАСТИ КОЈОМ ЈЕ
    ОДБИЈЕН ИЛИ ОДБАЧЕН ЗАХТЕВ ЗА ПРИСТУП ИНФОРМАЦИЈИ
    </vrsta_zalbe>
     <poverenik>Поверенику за информације од јавног значаја и заштиту података о личности
     </poverenik>
     <adresa_za_postu>
       <grad>Београд</grad>
       <ulica>Булевар краља Александрa</ulica>
       <broj>15</broj>
     </adresa_za_postu>
     <naslov>Ж А Л Б А
     </naslov>
     <informacije_o_zaliocu>
       <fizicko_lice>
       <ime_zalioca property="pred:ime_zalioca" datatype="xs:string">${this.ime}</ime_zalioca>
         <prezime_zalioca property="pred:prezime_zalioca" datatype="xs:string">${this.prezime}</prezime_zalioca> 
         <adresa_zalioca property="pred:adresa_zalioca" datatype="ns1:TAdresa">
           <grad>Novi Sad</grad>
           <ulica>Vojvodjanska</ulica>
           <broj>10</broj>
         </adresa_zalioca>
       </fizicko_lice>
     </informacije_o_zaliocu>
     <odluka_koja_se_pobija>
     <naziv_organa property="pred:naziv_organa" datatype="xs:string">
       </naziv_organa>
       <broj_resenja property="pred:broj_resenja" datatype="xs:string">
       </broj_resenja>
       <datum_resenja property="pred:datum_resenja" datatype="ns1:TDatum">
       <dan>${new Date().getDay()}</dan>
       <mesec>${new Date().getMonth()+1}</mesec>
       <godina>${new Date().getFullYear()}</godina>
       </datum_resenja>
     </odluka_koja_se_pobija>
     <razlog_zalbe>
     <datum_odbijenog_zahteva >
     <dan>${this.data.datum.getDay().toString()}</dan>
     <mesec>${(this.data.datum.getMonth()).toString()}</mesec>
     <godina>${this.data.datum.getFullYear().toString()}</godina>
   </datum_odbijenog_zahteva><sporni_dio property="pred:sporni_dio" datatype="xs:string">
   којим се захтев одбија на основу жалбе
   </sporni_dio>
   </razlog_zalbe>
   <podaci_o_zalbi>
       <mesto>Neko</mesto>
       <datum_zalbe property="pred:datum_zalbe" datatype="ns1:TDatum">
         <dan>${new Date().getDay()}</dan>
         <mesec>${new Date().getMonth()+1}</mesec>
         <godina>${new Date().getFullYear()}</godina>
       </datum_zalbe>
     </podaci_o_zalbi>
     <informacije_podnosiocu_zalbe>
        <ime_i_prezime_podnosioca property="pred:ime_i_prezime_podnosioca" datatype="xs:string">Пера Перић</ime_i_prezime_podnosioca>
          <adresa_podnosioca>
           <grad>NoviSad</grad>
           <ulica>Vojvodjanska</ulica>
           <broj>10</broj>
         </adresa_podnosioca>
       <podaci_za_kontakt>${this.email}</podaci_za_kontakt>
       <potpis_podnosioca/>
     </informacije_podnosiocu_zalbe>
     <napomena>
     <tacka>У жалби се мора навести одлука која се побија (решење, закључак, обавештење), назив органа који је одлуку донео, као и број и датум одлуке. Довољно је да жалилац наведе у жалби у ком погледу је незадовољан одлуком, с тим да жалбу не мора посебно образложити. Ако жалбу изјављује на овом обрасцу, додатно образложење може посебно приложити.
     </tacka>
     <tacka>Уз жалбу обавезно приложити копију поднетог захтева и доказ о његовој предаји-упућивању органу као и копију одлуке органа која се оспорава жалбом.
     </tacka>
     </napomena>
   </zalba>
    `;
    console.log(xmlString);
    let specification = this.xonomyService.appealDecisionXonomy;
    Xonomy.setMode("laic");
    Xonomy.render(xmlString, element, specification);
  }
  nastavi() {
    console.log(this.zalba_zbog);
    this.setuj = false;
    this.insertDataSilenceXonomy();
  }
  nastavi_sa_rjesenjem() {
    console.log("nastavak");
    this.setovano_rjesenje = true;
    this.unijetiPodaci = true;
    this.insertDataDecisionCreateXonomy();
  }

  createAppealOnDecision() {
    let harvest = Xonomy.harvest();
    console.log(harvest);
    if (this.data.isDecision) {
      this.serviceDecision.createAppealOnDecision(harvest).subscribe((response) => {
        this.notificationService.openSnackBar("Uspešno ste kreirali žalbu!");
      }, error => {
        this.notificationService.openSnackBar("Došlo je do greške. Proverite da li ste ispravno uneli sve");
      }
      );
    } else if (!this.data.isDecision) {
      console.log("sace posalti");
      console.log("zalba je zbog");

      console.log((this.zalba_zbog === 'nije postupio').toString());
      console.log((this.zalba_zbog === 'nije postupio u celosti').toString());
      console.log((this.zalba_zbog === 'u zakonskom roku').toString());
      if (!this.data.isDecision) {
        this.serviceDecision.createAppealOnSilence(harvest).subscribe((response) => {
          this.notificationService.openSnackBar("Uspešno ste kreirali žalbu!");
        }, error => {
          this.notificationService.openSnackBar("Došlo je do greške. Proverite da li ste ispravno uneli sve");
        }
        );
      }
    }
  }
  createDecision() {
    console.log("de smooooo");
    let harvest = Xonomy.harvest();
    this.serviceDecision.createDecision(harvest).subscribe((response) => {
      this.notificationService.openSnackBar("Uspešno ste kreirali rešenje!");
    }, error => {
      this.notificationService.openSnackBar("Došlo je do greške. Proverite da li ste ispravno uneli sve");
    }
    );
  }

  insertDataSilenceXonomy() {
    let element = document.getElementById("showXonomy");
    let xmlString =
      `<zalba xmlns="http://www.euprava.poverenik.gov.rs/appealSilence" xmlns:pred="http://www.euprava.poverenik.gov.rs/predicate/">   
     <naslov>ЖАЛБА КАДА ОРГАН ВЛАСТИ НИЈЕ ПОСТУПИО/ није поступио у целости/ ПО ЗАХТЕВУ ТРАЖИОЦА У ЗАКОНСКОМ РОКУ (ЋУТАЊЕ УПРАВЕ)
     </naslov>
     <podaci_o_primaocu>
         <kome>
         Повереникy за информације од јавног значаја и заштиту података о личности
         </kome>
         <adresa_za_postu>
             <grad>Београд</grad>
             <ulica>Булевар краља Александрa</ulica>
             <broj>15</broj>
         </adresa_za_postu>    
     </podaci_o_primaocu>
     <sadrzaj>
         <naziv_organa property="pred:naziv_organa" datatype="xs:string">Орган за жалбе</naziv_organa>
         <razlog_zalbe property="pred:razlog_zalbe" datatype="xs:string"> 
             <opcija izabrano="${(this.zalba_zbog === 'nije postupio') ? 'true' : 'false'}">nije postupio</opcija>
             <opcija izabrano="${(this.zalba_zbog === 'nije postupio u celosti').toString()}">nije postupio u celosti</opcija>
             <opcija izabrano="${(this.zalba_zbog === 'u zakonskom roku').toString()}"> u zakonskom roku</opcija>
         </razlog_zalbe>
         <datum_podnosenja_zahteva property="pred:datum_podnosenja_zahteva" datatype="ns1:TDatum">
         <dan>${this.data.datum.getDay().toString()}</dan>
         <mesec>${(this.data.datum.getMonth() + 1).toString()}</mesec>
         <godina>${this.data.datum.getFullYear().toString()}</godina>
         </datum_podnosenja_zahteva> 
         <podaci_o_zahtevu_i_informacijama></podaci_o_zahtevu_i_informacijama>
     </sadrzaj>
         <podnosilac_zalbe>
             <ime_i_prezime property="pred:ime_i_prezime" datatype="xs:string">${this.ime} ${this.prezime}</ime_i_prezime>
             <potpis/>
             <adresa>
                 <grad>Ужице</grad>
                 <ulica>Омладинска</ulica>
                 <broj>2</broj>
             </adresa>
             <drugi_podaci_za_kontakt/>      
         </podnosilac_zalbe>
         <podaci_o_zalbi>
           <mesto>Uzice</mesto>
           <dan>${new Date().getDay()}</dan>
           <mesec>${new Date().getMonth() + 1}</mesec>
           <godina>${new Date().getFullYear()}</godina>
         </podaci_o_zalbi>
     </zalba>`;
    console.log(xmlString);
    let specification = this.xonomyService.appealSilenceXonomy;
    Xonomy.setMode("laic");
    Xonomy.render(xmlString, element, specification);
  }

  insertDataDecisionCreateXonomy() {
    let element = document.getElementById("showXonomy");
    let xmlString = `<resenje xmlns="http://www.euprava.poverenik.gov.rs/decision" xmlns:pred="http://www.euprava.poverenik.gov.rs/predicate/">
 <broj_resenja property="pred:broj_resenja">071-1-1114-2020-03</broj_resenja>
 <uvod>
   <podnosilac_zalbe property="pred:podnosilac_zalbe"
     datatype="TOsoba">
     <ime>Pera</ime>
     <prezime>Peric</prezime>
   </podnosilac_zalbe>
   <razlog_zalbe>због непоступања Учитељског факултета у Призрену са привременим седиштем у Лепосавићу, ул. Немањина бб</razlog_zalbe>
   <datum_zahteva>12.12.2012</datum_zahteva>
   <zakon></zakon>
 </uvod>
 <tekst_resenja>
   <tekst>${this.rjesenje_opcija}... да без одлагања, а најкасније у року од пет дана од дана пријема овог решења, обавести АА, да ли поседује тражене информације односно документ у коме су исте садржане, и то: Уговор о раду који је као последњи потписан између тог Факултета и ББ, те да му, уколико такав документ поседује достави копију истог електронском поштом на адресу … или поштом, с тим што ће пре достављања заштитити и учинити недоступним податке о личности којима би се повредило право на приватност лица на које се информације односе, као што су: адреса становања, лични матични број грађана, име оца, радни стаж, просечна оцена студирања и сл. уколико су такви подаци садржани у траженом документу.
II О извршењу решења Учитељски факултет у Призрену са привременим седиштем у Лепосавићу, ће обавестити Повереника у року од седам дана од пријема овог решења.</tekst>
   <primalac_resenja property="pred:primalac_resenja">Univerzitet u Novom Sadu</primalac_resenja>
 </tekst_resenja>

 <tekst_obrazlozenja>
   АА, као тражилац информација, изјавио је дана 07.05.2020. године жалбу Поверенику, због непоступања Учитељског факултета у Призрену са привременим седиштем у Лепосавићу по његовом захтеву од 16.04.2020. године за приступ информацијама од јавног значаја и у прилогу доставио копију истог.
Поступајући по жалби, Повереник је дана 11.05.2020. године упутио исту на изјашњење Учитељском факултету у Призрену са привременим седиштем у Лепосавићу, као органу власти у смислу члана 3. Закона о слободном приступу информацијама од јавног значаја и затражио да се изјасни о наводима жалбе, посебно о разлозима непоступања у законском року по поднетом захтеву у складу са одредбама члана 16. ст.1-9. или ст. 10. Закона, остављајући рок од осам дана, поводом чега није добио одговор.
По разматрању жалбе и осталих списа овог предмета, донета је одлука као у диспозитиву решења из следећих разлога:
Увидом у списе предмета утврђено је да је АА, дана 16.04.2020. године, поднео Учитељском факултету у Призрену са привременим седиштем у Лепосавићу, захтев за приступ информацијама од јавног значаја, електронском поштом, којим је тражио информације од јавног значаја, ближе наведенe у ставу I диспозитива овог решења.
 </tekst_obrazlozenja>

 <poverenik property="pred:poverenik" datatype="TOsoba">
   <ime>${this.ime}</ime>
   <prezime>${this.prezime}</prezime>
 </poverenik>

</resenje>`;
    let specification = this.xonomyService.appealDecisionCreateXonomy;
    Xonomy.setMode("laic");
    Xonomy.render(xmlString, element, specification);
  }
}
