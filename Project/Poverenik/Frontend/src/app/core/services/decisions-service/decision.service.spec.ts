/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { DecisionService } from './decision.service';

describe('Service: Decision', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DecisionService]
    });
  });

  it('should ...', inject([DecisionService], (service: DecisionService) => {
    expect(service).toBeTruthy();
  }));
});
