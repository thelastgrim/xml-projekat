/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { XonomyService } from './xonomy.service';

describe('Service: Xonomy', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [XonomyService]
    });
  });

  it('should ...', inject([XonomyService], (service: XonomyService) => {
    expect(service).toBeTruthy();
  }));
});
