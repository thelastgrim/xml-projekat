import { Injectable } from "@angular/core";

declare const Xonomy: any;

@Injectable({
  providedIn: "root",
})
export class XonomyService {
  constructor() {}
  
  public appealDecisionCreateXonomy = {
    validate: function (jsElement) {
      //Validate the element:
      let elementSpec = this.elements[jsElement.name];
      if (elementSpec.validate) elementSpec.validate(jsElement);
      //Cycle through the element's attributes:
      for (let i = 0; i < jsElement.attributes.length; i++) {
        let jsAttribute = jsElement.attributes[i];
        let attributeSpec = elementSpec.attributes[jsAttribute.name];
        if (attributeSpec.validate) attributeSpec.validate(jsAttribute);
      }
      //Cycle through the element's children:
      for (let i = 0; i < jsElement.children.length; i++) {
        let jsChild = jsElement.children[i];
        if (jsChild.type == "element") {
          //if element
          this.validate(jsChild); //recursion
        }
      }
    },
    
    elements: 
    {
    
      zakon:{
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
            htmlID: jsElement.htmlID,
            text: "Ovo polje mora biti popunjeno!",
          });
        }
      },
      hasText: true,
      asker: Xonomy.askString,
    },
      broj_resenja: {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      ime : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      prezime : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      razlog_zalbe : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },datum_zahteva : {
        
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,

      }, tekst : {
        
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      }, primalac_resenja : {
        
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      tekst_obrazlozenja : {
        
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      }, 
      poverenik : {
        isReadOnly : true
      }
     }

  }

  public appealDecisionXonomy = {
    validate: function (jsElement) {
      //Validate the element:
      let elementSpec = this.elements[jsElement.name];
      if (elementSpec.validate) elementSpec.validate(jsElement);
      //Cycle through the element's attributes:
      for (let i = 0; i < jsElement.attributes.length; i++) {
        let jsAttribute = jsElement.attributes[i];
        let attributeSpec = elementSpec.attributes[jsAttribute.name];
        if (attributeSpec.validate) attributeSpec.validate(jsAttribute);
      }
      //Cycle through the element's children:
      for (let i = 0; i < jsElement.children.length; i++) {
        let jsChild = jsElement.children[i];
        if (jsChild.type == "element") {
          //if element
          this.validate(jsChild); //recursion
        }
      }
    },
    
    elements: {
      vrsta_zalbe: {
        isReadOnly: true,
      }, 
        poverenik: {
        isReadOnly: true,
      }, 
      adresa_za_postu: {
        isReadOnly: true,
      }, 
      naslov: {
        isReadOnly: true,
      },
      ime_zalioca: {
        isReadOnly: true,
      },
      prezime_zalioca: {
        isReadOnly: true,
      },
      grad  : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      }, 
      ulica : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      broj : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      mesto: {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      sporni_dio : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      napomena: {
        isReadOnly: true,
      },
      naziv_organa: {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },    
        broj_resenja: {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      datum_zahteva: {
        isReadOnly: true,
      },
      dan: {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      mesec: {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
       godina: {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      
    },
  };

  public appealSilenceXonomy = {
    validate: function (jsElement) {
      //Validate the element:
      let elementSpec = this.elements[jsElement.name];
      if (elementSpec.validate) elementSpec.validate(jsElement);
      //Cycle through the element's attributes:
      for (let i = 0; i < jsElement.attributes.length; i++) {
        let jsAttribute = jsElement.attributes[i];
        let attributeSpec = elementSpec.attributes[jsAttribute.name];
        if (attributeSpec.validate) attributeSpec.validate(jsAttribute);
      }
      //Cycle through the element's children:
      for (let i = 0; i < jsElement.children.length; i++) {
        let jsChild = jsElement.children[i];
        if (jsChild.type == "element") {
          //if element
          this.validate(jsChild); //recursion
        }
      }
    },
    elements: {
      podaci_o_primaocu :{
        isReadOnly : true,
      },
      naslov :{
        isReadOnly : true,
      },
      razlog_zalbe :{
        isInvisible : true,
      },
      datum_podnosenja_zahteva :{
        isReadOnly : true,
      },podaci_o_zahtevu_i_informacijama : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },
      ime_i_prezime : {
        isReadOnly : true,
      },grad : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },ulica : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },broj : {
        validate: function (jsElement) {
          if (jsElement.getText() == "") {
            Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovo polje mora biti popunjeno!",
            });
          }
        },
        hasText: true,
        asker: Xonomy.askString,
      },


    },
  };
}
