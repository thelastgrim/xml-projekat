import { Component, Input, OnChanges, OnInit, ViewChild } from "@angular/core";
import { MatPaginator, MatSort, MatTableDataSource } from "@angular/material";
import { Title } from "@angular/platform-browser";
import { NGXLogger } from "ngx-logger";
import { AppealService } from "src/app/core/services/appeals-service/appeal.service";
import { NotificationService } from "src/app/core/services/notification.service";
import { AppealDecision } from "src/app/model/appeal-Decision";

@Component({
  selector: "app-appeal-show",
  templateUrl: "./appeal-show.component.html",
  styleUrls: ["./appeal-show.component.css"],
})
export class AppealShowComponent implements OnChanges {
  displayedColumns: string[] = [
    "id",
    "podnosilac",
    "organ",
    "datum",
    "mesto",
    "status",
    "preuzmi",
    "izvezi"
  ];
  pagNumber: number;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private logger: NGXLogger,
    private notificationService: NotificationService,
    private service: AppealService,
    private titleService: Title
  ) {
    this.pagNumber = 0;
  }

  @Input()
  public showData: MatTableDataSource<any>;

  ngOnChanges() {
    this.titleService.setTitle(
      "Žalbe na odbijanje zahteva za informacije od javnog značaja"
    );
    if (typeof this.showData != "undefined") {
      this.logger.log("Customers loaded");
      this.showData.sort = this.sort;
      this.showData.paginator = this.paginator;
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.showData.filter = filterValue.trim().toLowerCase();

    if (this.showData.paginator) {
      this.showData.paginator.firstPage();
    }
  }
  preuzmiPDF(element: any) {
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "PDF").subscribe((response) => {
        console.log("da vidimo");
        let file = new Blob([response], { type: "application/pdf" });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.pdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "PDF").subscribe((response) => {
        let file = new Blob([response], { type: "application/pdf" });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.pdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");
    }
  }
  preuzmXHTML(element: any) {
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "HTML").subscribe((response) => {
        let file = new Blob([response], { type: 'text/html' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.html`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "HTML").subscribe((response) => {
        let file = new Blob([response], { type: 'text/html' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.html`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
    }
  }
  preuzmiJSON(element : any){
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "JSON").subscribe((response) => {
        let file = new Blob([response], { type: 'application/JSON' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.json`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u JSON formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "JSON").subscribe((response) => {
        let file = new Blob([response], { type: 'application/JSON' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.json`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste izvezli dokument u JSON formatu!");
    }
  }
  preuzmiRDF(element:any){
    if (element.id.split("-")[0] === "RJ") {
      this.service.exportDecision(element.id, "RDF").subscribe((response) => {
        let file = new Blob([response], { type: 'application/RDF' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.rdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u RDF formatu!");
    } else if (element.id.split("-")[0] === "RS") {
      this.service.exportSilence(element.id, "RDF").subscribe((response) => {
        let file = new Blob([response], { type: 'application/RDF' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.rdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste izvezli dokument u RDF formatu!");
    }
  }
}
