import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { AppealService } from 'src/app/core/services/appeals-service/appeal.service';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { AppealDecision } from 'src/app/model/appeal-Decision';
import * as txml from 'txml';

@Component({
  selector: 'app-appeals-silence',
  templateUrl: './appeals-silence.component.html',
  styleUrls: ['./appeals-silence.component.css']
})


export class AppealsSilenceComponent implements OnInit {

  data: MatTableDataSource<any>;
  dataUnresolved: MatTableDataSource<any>;
  dataCitizien: MatTableDataSource<any>;
  appeals: AppealDecision;
  isAdmin: boolean;

  constructor(
    private appealsService: AppealService,
    private authService: AuthenticationService,
    private authGuard: AuthGuard
  ) { }

  ngOnInit() {
    const user = this.authService.getCurrentUser();

    this.isAdmin = user.isAdmin;
    if (this.isAdmin) {
      this.appealsService.getAppealsSilence().subscribe((appeals) => {
        let obj: any = txml.parse(appeals);
        let requests: any = txml.simplify(obj);
        this.extractResolved(requests);
      });
      setTimeout(() => {

      this.appealsService.getAppealdSilenceUnresolved().subscribe((appeals) => {
        let obj: any = txml.parse(appeals);
        let requests: any = txml.simplify(obj);
        this.extractUnresolved(requests);
      });
    },
    100);
    } else {

      console.log("gradjanin");
      this.appealsService.getAppealdSilenceCitizien().subscribe((appeals) => {
        let obj: any = txml.parse(appeals);
        let requests: any = txml.simplify(obj);
        this.extractCitizien(requests);
        console.log("gradjanin");
        console.log(requests);
      });
    }
  }

  extractCitizien(requests: any) {
    let tableReq: any[] = [];
    let number = 1;
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
      let request = {
        'id': 'RS' + requests.List.item.about.split('/RS')[1],
        'podnosilac': requests.List.item.podnosilacZalbe.imeIPrezime.value,
        'mesto': requests.List.item.podaciOZalbi.mesto,
        'datum': new Date(requests.List.item.podaciOZalbi.godina, requests.List.item.podaciOZalbi.mesec-1, requests.List.item.podaciOZalbi.dan),
        'organ': requests.List.item.sadrzaj.nazivOrgana.value,
        'status': (requests.List.item.about.split('R')[1].split('-')[0] === "SP") ? "Rešeno" : "Nerešeno"
      }
     tableReq.push(request);
     this.dataCitizien =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    requests.List.item.forEach(req => {
      console.log(req);
      let request = {
        'id': 'RS' + req.about.split('/RS')[1],
        'podnosilac': req.podnosilacZalbe.imeIPrezime.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum': new Date(req.podaciOZalbi.godina, req.podaciOZalbi.mesec-1, req.podaciOZalbi.dan),
        'organ': req.sadrzaj.nazivOrgana.value,
        'status': (req.about.split('R')[1].split('-')[0] === "SP") ? "Rešeno" : "Nerešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.dataCitizien = new MatTableDataSource(tableReq);
  }
}

  extractResolved(requests: any) {
    let tableReq: any[] = [];
    let number = 1;
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
      let request = {
        'id': 'RS' + requests.List.item.about.split('/RS')[1],
        'podnosilac': requests.List.item.podnosilacZalbe.imeIPrezime.value,
        'mesto': requests.List.item.podaciOZalbi.mesto,
        'datum': new Date(requests.List.item.podaciOZalbi.godina, requests.List.item.podaciOZalbi.mesec, requests.List.item.podaciOZalbi.dan),
        'organ': requests.List.item.sadrzaj.nazivOrgana.value,
        'status': "rešeno"
      }
     tableReq.push(request);
     this.data =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    requests.List.item.forEach(req => {
      console.log(req);
      let request = {
        'id': 'RS' + req.about.split('/RS')[1],
        'podnosilac': req.podnosilacZalbe.imeIPrezime.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum': new Date(req.podaciOZalbi.godina, req.podaciOZalbi.mesec, req.podaciOZalbi.dan),
        'organ': req.sadrzaj.nazivOrgana.value,
        'status': "rešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.data = new MatTableDataSource(tableReq);
  }
}

  extractUnresolved(requests: any) {
    let tableReq: any[] = [];
    let number = 1;
    console.log(requests.List.item[0]);
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
      let request = {
        'id': 'RS' + requests.List.item.about.split('/RS')[1],
        'podnosilac': requests.List.item.podnosilacZalbe.imeIPrezime.value,
        'mesto': requests.List.item.podaciOZalbi.mesto,
        'datum': new Date(requests.List.item.podaciOZalbi.godina, requests.List.item.podaciOZalbi.mesec, requests.List.item.podaciOZalbi.dan),
        'organ': requests.List.item.sadrzaj.nazivOrgana.value,
        'status': "nerešeno"
      }
     tableReq.push(request);
     this.dataUnresolved =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    requests.List.item.forEach(req => {
      console.log(req);
      let request = {
        'id': 'RS' + req.about.split('/RS')[1],
        'podnosilac': req.podnosilacZalbe.imeIPrezime.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum': new Date(req.podaciOZalbi.godina, req.podaciOZalbi.mesec, req.podaciOZalbi.dan),
        'organ': req.sadrzaj.nazivOrgana.value,
        'status': "nerešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.dataUnresolved = new MatTableDataSource(tableReq);
  }
}
}
