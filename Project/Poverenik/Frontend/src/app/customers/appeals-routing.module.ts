import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutComponent } from '../shared/layout/layout.component';
import { AppealsRejection } from './appeals-rejection-list/appeals-rejection-list.component';
import { AppealShowComponent} from 'src/app/customers/appeal-show/appeal-show/appeal-show.component';
import { AppealsSilenceComponent } from './appeals-silence/appeals-silence-list/appeals-silence/appeals-silence.component';

const routes: Routes = [
  {
    path: 'rejeciton',
    component: LayoutComponent,
    children: [
      { path: '', component: AppealsRejection },
    ]
  },
  {
    path: 'silence',
    component: LayoutComponent,
    children: [
      { path: '', component: AppealsSilenceComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealsRoutingModule { }
