import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { DecisionListComponent } from './decision-list/decision-list/decision-list.component';
import { DecisionShowComponent } from './decision-show/decision-show/decision-show.component';
import { DecisionsRoutingModel } from './decisions-routing.module';

@NgModule({
  imports: [
    CommonModule,
    DecisionsRoutingModel,
    SharedModule
  ],
  declarations: [
    DecisionListComponent,
    DecisionShowComponent
  ],
  entryComponents: [
  ]
})
export class DecisionsModule { }
