import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { Title } from '@angular/platform-browser';
import { NGXLogger } from 'ngx-logger';
import { DecisionService } from 'src/app/core/services/decisions-service/decision.service';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-decision-show',
  templateUrl: './decision-show.component.html',
  styleUrls: ['./decision-show.component.css']
})
export class DecisionShowComponent implements OnChanges {

  displayedColumns: string[] = ['id', 'podnosilac', 'poverenik', 'datum', 'izvezi', 'preuzmi'];
  pagNumber : number;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(
    private logger: NGXLogger,
    private notificationService: NotificationService,
    private titleService: Title,
    private decisionService : DecisionService) {
       this.pagNumber = 0;
      }

  @Input()
  public showData :  MatTableDataSource<any>;

  ngOnChanges() {
    this.titleService.setTitle('Žalbe na odbijanje zahteva za informacije od javnog značaja'); 
    console.log("Ispisujemo sada sva rjesenja!");
    if(typeof(this.showData)!= "undefined"){
        this.logger.log('Customers loaded');
        this.showData.sort = this.sort;
        this.showData.paginator = this.paginator;
    }
  }

     
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.showData.filter = filterValue.trim().toLowerCase();

    if (this.showData.paginator) {
      this.showData.paginator.firstPage();
    }
  }
  preuzmiPDF(element : any){
    this.decisionService.export(element.id, "PDF").subscribe((response) => {
      let file = new Blob([response], { type: "application/pdf" });
      var fileURL = URL.createObjectURL(file);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = fileURL;
      a.download = `Rešena žalba broj ${element.id}.pdf`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notificationService.openSnackBar("Desila se greška!");
      () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");
  }

  preuzmXHTML(element : any){
    this.decisionService.export(element.id, "HTML").subscribe((response) => {
      let file = new Blob([response], { type: 'text/html' });
      var fileURL = URL.createObjectURL(file);
      let a = document.createElement("a");
      document.body.appendChild(a);
      a.setAttribute("style", "display: none");
      a.href = fileURL;
      a.download = `Rešena žalba broj ${element.id}.html`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notificationService.openSnackBar("Desila se greška!");
      () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
  }

  preuzmiRDF(element:any){
    // if (element.id.split("-")[0] === "RJ") {
      this.decisionService.export(element.id, "RDF").subscribe((response) => {
        let file = new Blob([response], { type: 'application/RDF' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.rdf`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u RDF formatu!");
    // }
  }
  preuzmiJSON(element : any){
    // if (element.id.split("-")[0] === "RJ") {
      this.decisionService.export(element.id, "JSON").subscribe((response) => {
        let file = new Blob([response], { type: 'application/JSON' });
        var fileURL = URL.createObjectURL(file);
        let a = document.createElement("a");
        document.body.appendChild(a);
        a.setAttribute("style", "display: none");
        a.href = fileURL;
        a.download = `Rešena žalba broj ${element.id}.json`;
        a.click();
        window.URL.revokeObjectURL(fileURL);
        a.remove();
      }),
        (error) => this.notificationService.openSnackBar("Desila se greška!");
        () => this.notificationService.openSnackBar("Uspješno ste skinuli dokument u JSON formatu!");
    // }
  }
}
