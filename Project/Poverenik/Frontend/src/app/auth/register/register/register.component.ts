import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { ValidationError } from 'ajv';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { NotificationService } from 'src/app/core/services/notification.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registerForm : FormGroup;
  loading: boolean;

  constructor(private router: Router,
      private titleService: Title,
      private notificationService : NotificationService,
      private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.titleService.setTitle('Register');
    this.authenticationService.logout();
    this.createForm();
  }

  createForm() {
    const savedUserEmail = localStorage.getItem('savedUserEmail');

    this.registerForm = new FormGroup({
      email : new FormControl(savedUserEmail, [Validators.required, Validators.email]),
      first_name : new FormControl('', [Validators.pattern('[a-zA-Z]*'), Validators.required, Validators.minLength(2), Validators.maxLength(24)]),
      last_name :  new FormControl('', [Validators.pattern('[a-zA-Z]*'), Validators.required, Validators.minLength(2), Validators.maxLength(24)]),
      password: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(18)]),
      confirmPassword:new FormControl('', [Validators.required]),
      rememberMe : new FormControl(savedUserEmail !== null)
    });

  }

register() {
  const email = this.registerForm.get('email').value;
  const password = this.registerForm.get('password').value;
  const confirmPassword = this.registerForm.get('confirmPassword').value;
  const first_name = this.registerForm.get('first_name').value;
  const last_name = this.registerForm.get('last_name').value;
  const rememberMe = this.registerForm.get('rememberMe').value;

  this.loading = true;
  if(this.checkPasswords(password, confirmPassword)){
    this.authenticationService
    .register(email.toLowerCase(), password, first_name, last_name)
    .subscribe(
        data => {
            if (rememberMe) {
                localStorage.setItem('savedUserEmail', email);
            } else {
                localStorage.removeItem('savedUserEmail');
            }
            this.router.navigate(['/']);
        },
        error => {
            this.notificationService.openSnackBar(error.error);
            this.loading = false;
        }
    );
  }else{
    this.notificationService.openSnackBar("Lozinke se ne podudaraju, pokusajte ponovo!");
    this.loading = false;
  }

}
resetPassword() {
  this.router.navigate(['/auth/password-reset-request']);
}
checkPasswords(password: FormControl,confirmPassword: FormControl) { // here we have the 'passwords' group
   return password === confirmPassword ? true : false;     
}
login(){
  this.router.navigate(['/auth/login']);
}
}
