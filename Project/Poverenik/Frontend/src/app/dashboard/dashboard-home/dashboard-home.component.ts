import { Component, OnInit } from '@angular/core';
import { NotificationService } from 'src/app/core/services/notification.service';
import { Title } from '@angular/platform-browser';
import { NGXLogger } from 'ngx-logger';
import { AuthenticationService } from 'src/app/core/services/auth.service';
import { DecisionService } from 'src/app/core/services/decisions-service/decision.service';
import { MatTableDataSource } from '@angular/material';
import * as txml from 'txml';
import { AppealService } from 'src/app/core/services/appeals-service/appeal.service';

@Component({
  selector: 'app-dashboard-home',
  templateUrl: './dashboard-home.component.html',
  styleUrls: ['./dashboard-home.component.css']
})
export class DashboardHomeComponent implements OnInit {
  isAdmin: boolean;
  currentUser: any;
  value : string;
  valueAppealSilence : string;
  valueAppealDecision : string;
  
  dataAppealDecision :  MatTableDataSource<any>;
  dataAppealSilence :  MatTableDataSource<any>;
  data : MatTableDataSource<any>;

  constructor(private notificationService: NotificationService,
    private authService: AuthenticationService,
    private decisionService : DecisionService,
    private appealService : AppealService,
    private titleService: Title,
    private logger: NGXLogger) {
  }

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();

    this.isAdmin = this.currentUser.isAdmin;
    // this.titleService.setTitle('angular-material-template - Dashboard');
    // this.logger.log('Dashboard loaded');

    setTimeout(() => {
      this.notificationService.openSnackBar('Welcome!');
    });
  }

  search(value : string){
    this.decisionService.searchDecision(value).subscribe((response) => {
      let obj: any = txml.parse(response);
      let requests: any = txml.simplify(obj);
      this.extractDecisions(requests);
      console.log(requests);
    }, error => {
      this.notificationService.openSnackBar("U bazi ne postoji dokument sa unešenim sadržajem!");
    }
    );
  }

  searchAppealDecision(value : string){
    
    console.log("search decision appeal");
      this.appealService.searchAppealDecision(value).subscribe((response) => {
      console.log("sta je rezulata");
      let obj: any = txml.parse(response);
      let requests: any = txml.simplify(obj);
      this.extractAppealDecision(requests);
      console.log("search decision appeal");
      console.log(requests);
    }, error => {
      this.notificationService.openSnackBar("U bazi ne postoji dokument sa unešenim sadržajem!");
    }
    );
    console.log(value);
  }

  searchAppealSilence(value :  string){
    this.appealService.searchAppealSilence(value).subscribe((response) => {
      let obj: any = txml.parse(response);
      let requests: any = txml.simplify(obj);
      this.exstractAppealSilence(requests);
      console.log("search silence");
      console.log(requests);
    }, error => {
      this.notificationService.openSnackBar("U bazi ne postoji dokument sa unešenim sadržajem!");
    }
    );
  }
  extractAppealDecision( requests : any){
    let tableReq : any[] = [];
    let number = 1;
    console.log(requests.List);
    // console.log(requests.List.item[0]);
   if(!requests.List.item[0]){
     console.log("tu sam");
     console.log(requests.List);
    let request = {
      'id' : requests.List.item.about.split('appealDecision/')[1],
      'podnosilac' : requests.List.item.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
      'mesto': requests.List.item.podaciOZalbi.mesto,
      'datum' :new Date(requests.List.item.odlukaKojaSePobija.datumResenja.godina,requests.List.item.odlukaKojaSePobija.datumResenja.mesec,requests.List.item.odlukaKojaSePobija.datumResenja.dan) ,
      'organ' : requests.List.item.odlukaKojaSePobija.nazivOrgana.value,
      'status' : (requests.List.item.about.split('R')[1].split('-')[0] === "JP") ? "Rešeno" : "Nerešeno"
    }
    tableReq.push(request);
    this.dataAppealDecision =new MatTableDataSource(tableReq);
    console.log(this.dataAppealDecision);
   }else{
    console.log("tu sam 2");
    requests.List.item.forEach(req =>{
      let request = {
        'id' : req.about.split('appealDecision/')[1],
        'podnosilac' : req.informacijePodnosiocuZalbe.imeIPrezimePodnosioca.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum' :new Date(req.odlukaKojaSePobija.datumResenja.godina,req.odlukaKojaSePobija.datumResenja.mesec,req.odlukaKojaSePobija.datumResenja.dan) ,
        'organ' : req.odlukaKojaSePobija.nazivOrgana.value,
        'status' : (req.about.split('R')[1].split('-')[0] === "JP") ? "Rešeno" : "Nerešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.dataAppealDecision =new MatTableDataSource(tableReq);
   }
    
  }
  exstractAppealSilence(requests: any) {
    let tableReq: any[] = [];
    let number = 1;
    console.log(requests.List.item[0]);
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
     let request = {
       'id' :'RS'+ requests.List.item.about.split('/RS')[1],
       'podnosilac' : requests.List.item.podnosilacZalbe.imeIPrezime.value,
       'mesto': requests.List.item.podaciOZalbi.mesto,
       'datum' :new Date(requests.List.item.podaciOZalbi.godina,requests.List.item.podaciOZalbi.mesec,requests.List.item.podaciOZalbi.dan) ,
       'organ' : requests.List.item.sadrzaj.nazivOrgana.value,
       'status' : (requests.List.item.about.split('R')[1].split('-')[0] === "SP") ? "Rešeno" : "Nerešeno"
     }
     tableReq.push(request);
     this.dataAppealSilence =new MatTableDataSource(tableReq);
     console.log(this.dataAppealDecision);
    }else{
    requests.List.item.forEach(req => {
      console.log(req);
      let request = {
        'id': 'RS' + req.about.split('/RS')[1],
        'podnosilac': req.podnosilacZalbe.imeIPrezime.value,
        'mesto': req.podaciOZalbi.mesto,
        'datum': new Date(req.podaciOZalbi.godina, req.podaciOZalbi.mesec, req.podaciOZalbi.dan),
        'organ': req.sadrzaj.nazivOrgana.value,
        'status': (req.about.split('R')[1].split('-')[0] === "SP") ? "Rešeno" : "Nerešeno"
      }
      tableReq.push(request);
      number++;
    });
    this.dataAppealSilence = new MatTableDataSource(tableReq);
  }
  }

  extractDecisions( requests : any){
    let tableReq : any[] = [];
    let number = 1;
    console.log("exstract");
    if(!requests.List.item[0]){
      console.log("tu sam");
      console.log(requests.List);
     let request = {
      'id' : requests.List.item.brojResenja.value,
        'podnosilac' : requests.List.item.uvod.podnosilacZalbe.ime + " " + requests.List.item.uvod.podnosilacZalbe.prezime,
        'datum' : requests.List.item.datum.split('године')[0] ,
        'poverenik' : requests.List.item.poverenik.ime + " "  + requests.List.item.poverenik.prezime
       }
     tableReq.push(request);
     this.data =new MatTableDataSource(tableReq);
     console.log(this.data);
    }else{
    console.log((requests.List.item.length));
    requests.List.item.forEach(req =>{
      let request = {
        'id' : req.brojResenja.value,
        'podnosilac' : req.uvod.podnosilacZalbe.ime + " " + req.uvod.podnosilacZalbe.prezime,
        'datum' : req.datum.split('године')[0] ,
        'poverenik' : req.poverenik.ime + " "  + req.poverenik.prezime
      }
      tableReq.push(request);
      number++;
    });
    this.data =new MatTableDataSource(tableReq);
  }
}

}
