package com.poverenik.controler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.xmldb.api.base.XMLDBException;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.poverenik.dto.DTOUser;
import com.poverenik.dto.DTOUserLogin;
import com.poverenik.dto.DTOUserSignUp;
import com.poverenik.dto.DTOUserTokenState;
import com.poverenik.model.user.User;
import com.poverenik.security.TokenUtils;
import com.poverenik.service.UserService;
import com.poverenik.util.Person;


@RestController
@RequestMapping(value = "/auth",
				produces = MediaType.APPLICATION_XML_VALUE,
				consumes = MediaType.APPLICATION_XML_VALUE)
public class AuthenticationController {
	
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private TokenUtils tokenUtils;
	 
	
	@Autowired
    private AuthenticationManager authenticationManager;
	
	private BCryptPasswordEncoder encoder;
	private XmlMapper mapper;
	
	
	@PostMapping("/sign-up")
    public ResponseEntity<?> addUser(@RequestBody DTOUserSignUp userRequest) throws Exception {
        User existUser = this.userService.findByEmail(userRequest.getEmail());
        
        if (existUser != null) {
        	 return new ResponseEntity<>("Email already exists", HttpStatus.NOT_ACCEPTABLE);
        }

        try {
        	encoder = new BCryptPasswordEncoder();
        	mapper = new XmlMapper();
        	System.out.println(userRequest.getPassword() + " ==PASSWORD");
        	DTOUser dtoUser = new DTOUser(userRequest.getFirst_name(),
        								  userRequest.getLast_name(),
        								  userRequest.getEmail(),
        								  encoder.encode(userRequest.getPassword()),
        								  "ROLE_CITIZEN");
        	
        	this.userService.create(mapper.writeValueAsString(dtoUser).replace("DTOUser", "user"), userRequest.getEmail());
        	/*
        	User123123 u = userMapper.toEntity(userRequest);
            existUser = userService.create(u);
            String toEncrypt = existUser.getEmail()+existUser.getId();
            System.err.println("To encrypt: " + toEncrypt);
            EncryptDecrypt ed;
    	    String encrypted = null;
    	    ed = new EncryptDecrypt();
			encrypted = ed.encrypt(toEncrypt).replace("/", "culturalContent");
			System.err.println("encrypted: " + encrypted);
			SimpleMailMessage mailMessage = new SimpleMailMessage();
            mailMessage.setTo(u.getEmail());
            mailMessage.setSubject("Complete Registration!");
         //   mailMessage.setFrom("some@hotmail.com");
            mailMessage.setText("To confirm your account, please click here : "
                    +"http://localhost:8080/auth/confirm-account/"+encrypted);
            
          //posalji mejl
			new Thread(new Runnable() {
			    public void run() {
			    	 emailService.sendEmail(mailMessage);
			    }
			}).start();
           
    	    */
        } catch (Exception e) {
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
        
        
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
	
	@PostMapping("/log-in")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody DTOUserLogin authenticationRequest,
                                                                    HttpServletResponse response) throws JAXBException, XMLDBException {
		
		Authentication authentication = null;
		try {
			authentication = authenticationManager
	                .authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(),
	                        authenticationRequest.getPassword()));
		}catch (BadCredentialsException e) {
			Object o = "Neispravni podaci za prijavljivanje!";
			return new ResponseEntity<>(o,HttpStatus.BAD_REQUEST);
		}
        
	
		
		
        // Ubaci korisnika u trenutni security kontekst
        SecurityContextHolder.getContext().setAuthentication(authentication);
        // Kreiraj token za tog korisnika
        	


        // Kreiraj token za tog korisnika
        
        System.out.println("DOBAVLJEN KORISNIK");
        System.out.println(SecurityContextHolder.getContext().getAuthentication());
        
        User u = userService.findByEmail( ((Person) authentication.getPrincipal()).getEmail());
        
        String fullName = u.getFirstName()+" "+u.getLastName();
     
        String jwt = tokenUtils.generateToken(u.getEmail(), u.getRole(), fullName); // prijavljujemo se na sistem sa email adresom
        int expiresIn = tokenUtils.getExpiredIn();
            
        System.out.println("USER = "+SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    		
        // Vrati token kao odgovor na uspesnu autentifikaciju
		return ResponseEntity.ok(new DTOUserTokenState(jwt, expiresIn));
 
    }

}
