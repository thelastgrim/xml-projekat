package com.poverenik.security.auth;

import java.util.Collection;
import java.util.HashSet;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import org.xmldb.api.base.XMLDBException;

import com.poverenik.model.user.User;
import com.poverenik.repos.UserRepo;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider{
	
	@Autowired
	private UserRepo userRepo;
	
	private BCryptPasswordEncoder encoder;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		// TODO Auto-generated method stub
		return false;
	}
	
	
	/*
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		
		String email = authentication.getName();
		String password = authentication.getCredentials().toString();
				
		try {
			User user;
			
			 Collection<GrantedAuthority> grantedAuthoritySet = new HashSet<>();
			
			if((user =userRepo.findByEmail(email))!=null) {
				 encoder = new BCryptPasswordEncoder();
				 if(encoder.matches(password, user.getPassword())) {
					 if(user.getRole().equals("ROLE_CITIZEN")) {
						 grantedAuthoritySet.add(new SimpleGrantedAuthority("ROLE_CITIZEN"));
					 }else {
						 grantedAuthoritySet.add(new SimpleGrantedAuthority("ROLE_COMMISSIONER"));
					 }
					
					 return new UsernamePasswordAuthenticationToken(
				              email, password, grantedAuthoritySet);
				 }else {
					 throw new BadCredentialsException("Password doest not match.");
				 }
				
			}else {
				throw new BadCredentialsException("No user with that email.");
			}
		} catch (JAXBException | XMLDBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		 return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
*/
}
