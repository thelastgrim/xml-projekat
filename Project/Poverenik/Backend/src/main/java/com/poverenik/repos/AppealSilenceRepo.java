package com.poverenik.repos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.poverenik.database.ExistManager;
import com.poverenik.database.FusekiManager;
import com.poverenik.model.appealSilence.Zalba;
import com.poverenik.util.Constants;

@Repository
public class AppealSilenceRepo {
	
	@Autowired
	private ExistManager existManager;
	
	@Autowired
	private FusekiManager fusekiManager;
	
	private String query;
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	List<Zalba> requests;
	
	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/appealSilence";
	private final String APPEND = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:append select=\"%1$s\" child=\"last()\">%2$s</xu:append>"
			+ "</xu:modifications>";

	private final String UPDATE = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:update select=\"%1$s\">%2$s</xu:update>"
			+ "</xu:modifications>";
	
	public List<Zalba> getAll() throws XMLDBException, JAXBException {
		query = String.format("/zalbe_na_cutanje/*");
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		System.out.println(set.getSize());
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zalba>();
			context = JAXBContext.newInstance("com.poverenik.model.appealSilence");
			unmarshaller = context.createUnmarshaller();
			Zalba z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zalba) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
	
	}

	public void create(Zalba zalbaObj, String zalba) throws Exception {
		existManager.add(Constants.COLLECTION_URI,
				 Constants.ZALBACUTANJE_ID,
				 "/zalbe_na_cutanje",
				 zalba,
				 APPEND);
		fusekiManager.addAppealSilence(zalbaObj);
	}

	public List<Zalba> findAllByUserEmail(String email) throws XMLDBException, JAXBException {
		query = String.format("/zalbe_na_cutanje/zalba[@podnosilac='%s']", email);
		
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zalba>();
			context = JAXBContext.newInstance("com.poverenik.model.appealSilence");
			unmarshaller = context.createUnmarshaller();
			Zalba z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zalba) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
	}

	public String findById(String id) throws XMLDBException {
		query = String.format("/zalbe_na_cutanje/zalba[@id='%s']", id);
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() !=0) {
			
			return set.getResource(0).getContent().toString();
		}else {
			return null;
		}
		
		
	}

	public List<Zalba> findAllByContains(String text) throws XMLDBException, JAXBException {
		query = String.format("/zalbe_na_cutanje/zalba[podaci_o_primaocu/kome[contains(., '%1$s')] or "
				   + "adresa_za_postu/grad[contains(., '%1$s')] or "
				   + "sadrzaj/naziv_organa[contains(., '%1$s')] or "
				   + "sadrzaj/podaci_o_zahtevu_i_informacijama[contains(., '%1$s')] or "
				   + "podnosilac_zalbe/ime_i_prezime[contains(., '%1$s')] or "
				   + "podnosilac_zalbe/adresa/grad[contains(., '%1$s')] or "
				   + "podnosilac_zalbe/adresa/ulica[contains(., '%1$s')] or "
				   + "sadrzaj/podaci_o_zalbi/mesto[contains(., '%1$s')]]", text);




		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
		
			requests = new ArrayList<Zalba>();
			context = JAXBContext.newInstance("com.poverenik.model.appealSilence");
			unmarshaller = context.createUnmarshaller();
			Zalba z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zalba) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				
				requests.add(z);
			}
		
			return requests;	
		}
		else {
			return null;
		}
	}

}
