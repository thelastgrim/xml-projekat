package com.poverenik.controler;

import java.util.List;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.xmldb.api.base.XMLDBException;

import com.poverenik.model.requests.Zahtev;
import com.poverenik.service.RequestService;
import com.poverenik.util.Person;

@RestController

@RequestMapping(value = "/requests",
				//consumes = MediaType.APPLICATION_XML_VALUE,
				produces = MediaType.APPLICATION_XML_VALUE)
public class RequestController {
	
	@Autowired
	private RequestService requestService;
	
	@PreAuthorize("hasRole('ROLE_CITIZEN')")
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Zahtev>> getAllForUser() throws XMLDBException, JAXBException {
		
		List<Zahtev> requests = requestService.findAllByUserEmail(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		
		if(requests!=null) {
			return new ResponseEntity<List<Zahtev>>(requests, HttpStatus.OK);
		}else {
			System.out.println("nema ");
			return new ResponseEntity<List<Zahtev>>(requests, HttpStatus.NOT_FOUND);
		}

	}
	
	@PreAuthorize("hasRole('ROLE_CITIZEN')")
	@RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_XML_VALUE)
	public ResponseEntity<?> create(@RequestBody String zahtev) {
		
		
		try {
			this.requestService.create(zahtev);
			return new ResponseEntity<>(null, HttpStatus.OK);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
				
	}

}
