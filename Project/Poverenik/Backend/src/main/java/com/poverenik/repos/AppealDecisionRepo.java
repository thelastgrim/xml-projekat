package com.poverenik.repos;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.poverenik.database.ExistManager;
import com.poverenik.database.FusekiManager;
import com.poverenik.model.appealDecision.Zalba;
import com.poverenik.model.decision.Resenje;
import com.poverenik.model.requests.Zahtev;
import com.poverenik.util.Constants;

@Repository
public class AppealDecisionRepo {

	@Autowired
	private ExistManager existManager;
	@Autowired
	private FusekiManager fusekiManager;
	
	private String query;
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	List<Zalba> requests;
	
	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/appealDecision";
	private final String APPEND = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:append select=\"%1$s\" child=\"last()\">%2$s</xu:append>"
			+ "</xu:modifications>";

	private final String UPDATE = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:update select=\"%1$s\">%2$s</xu:update>"
			+ "</xu:modifications>";
	
	public List<Zalba> getAll() throws XMLDBException, JAXBException {
		query = String.format("/zalbe_na_odluku/*");
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		System.out.println(set.getSize());
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zalba>();
			context = JAXBContext.newInstance("com.poverenik.model.appealDecision");
			unmarshaller = context.createUnmarshaller();
			Zalba z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zalba) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
	
	}

	public void create(Zalba zalbaObj, String zalba) throws Exception {
		existManager.add(Constants.COLLECTION_URI,
				 Constants.ZALBAODLUKA_ID,
				 "/zalbe_na_odluku",
				 zalba,
				 APPEND);
		fusekiManager.addAppealDecision(zalbaObj);
	}

	public List<Zalba> findAllByUserEmail(String email) throws JAXBException, XMLDBException {
		
		query = String.format("/zalbe_na_odluku/zalba[@podnosilac='%s']", email);
		
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zalba>();
			context = JAXBContext.newInstance("com.poverenik.model.appealDecision");
			unmarshaller = context.createUnmarshaller();
			Zalba z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zalba) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
		
	}

	public String findById(String id) throws XMLDBException {
		String fullId = "http://www.euprava.poverenik.gov.rs/rdf/examples/appealDecision/"+id;
		query = String.format("/zalbe_na_odluku/zalba[@about='%s']", fullId);
		
									
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() !=0) {
			
			return set.getResource(0).getContent().toString();
		}else {
			return null;
		}
		
		
	}

	public List<Zalba> findAllByContains(String text) throws XMLDBException, JAXBException {
		query = String.format("/zalbe_na_odluku/zalba[informacije_o_zaliocu/fizicko_lice/ime_zalioca[contains(., '%1$s')] or "
                								   + "informacije_o_zaliocu/fizicko_lice/prezime_zalioca[contains(., '%1$s')] or "
                								   + "odluka_koja_se_pobija/naziv_organa[contains(., '%1$s')] or "
                								   + "razlog_zalbe/sporni_dio[contains(., '%1$s')] or "
                								   + "informacije_podnosiocu_zalbe/ime_i_prezime_podnosioca[contains(., '%1$s')] or "
                								   + "informacije_podnosiocu_zalbe/adresa_podnosioca/ulica[contains(., '%1$s')] or "
                								   + "informacije_podnosiocu_zalbe/adresa_podnosioca/grad[contains(., '%1$s')] or "
                								   + "podaci_o_zalbi/mesto[contains(., '%1$s')]]", text);




			ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
			
			if(set.getSize() != 0) {
			
				requests = new ArrayList<Zalba>();
				context = JAXBContext.newInstance("com.poverenik.model.appealDecision");
				unmarshaller = context.createUnmarshaller();
				Zalba z = null;
				for (int i = 0; i < set.getSize(); i++) {
					z = (Zalba) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
					
					requests.add(z);
				}
					
				return requests;	
				}
				else {
					return null;
				}
	}

}
