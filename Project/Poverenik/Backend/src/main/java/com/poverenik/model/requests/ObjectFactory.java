//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.02.02 at 05:25:52 PM CET 
//


package com.poverenik.model.requests;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the d package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ElektronskomPostom_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "elektronskom_postom");
    private final static QName _Postom_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "postom");
    private final static QName _DrugiNacin_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "drugi_nacin");
    private final static QName _Stav_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "stav");
    private final static QName _KopijaDokumenta_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "kopija_dokumenta");
    private final static QName _Broj_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "broj");
    private final static QName _PravnoLice_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "pravno_lice");
    private final static QName _Zakon_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "zakon");
    private final static QName _BrojClana_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "broj_clana");
    private final static QName _UvidUDokument_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "uvid_u_dokument");
    private final static QName _DrugiPodaciZaKontakt_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "drugi_podaci_za_kontakt");
    private final static QName _Grad_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "grad");
    private final static QName _Ulica_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "ulica");
    private final static QName _PosedovanjeInformacije_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "posedovanje_informacije");
    private final static QName _Faksom_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "faksom");
    private final static QName _InformacijeNaKojeSeOdnosiZahtev_QNAME = new QName("http://www.euprava.poverenik.gov.rs/request", "informacije_na_koje_se_odnosi_zahtev");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: d
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Ime }
     * 
     */
    public Ime createIme() {
        return new Ime();
    }

    /**
     * Create an instance of {@link FizickoLice }
     * 
     */
    public FizickoLice createFizickoLice() {
        return new FizickoLice();
    }

    /**
     * Create an instance of {@link Prezime }
     * 
     */
    public Prezime createPrezime() {
        return new Prezime();
    }

    /**
     * Create an instance of {@link Trazioc }
     * 
     */
    public Trazioc createTrazioc() {
        return new Trazioc();
    }

    /**
     * Create an instance of {@link NazivOrgana }
     * 
     */
    public NazivOrgana createNazivOrgana() {
        return new NazivOrgana();
    }

    /**
     * Create an instance of {@link Zahtev }
     * 
     */
    public Zahtev createZahtev() {
        return new Zahtev();
    }

    /**
     * Create an instance of {@link PodaciOOrganu }
     * 
     */
    public PodaciOOrganu createPodaciOOrganu() {
        return new PodaciOOrganu();
    }

    /**
     * Create an instance of {@link SedisteOrgana }
     * 
     */
    public SedisteOrgana createSedisteOrgana() {
        return new SedisteOrgana();
    }

    /**
     * Create an instance of {@link ZahtevNaOsnovu }
     * 
     */
    public ZahtevNaOsnovu createZahtevNaOsnovu() {
        return new ZahtevNaOsnovu();
    }

    /**
     * Create an instance of {@link ElementiZahteva }
     * 
     */
    public ElementiZahteva createElementiZahteva() {
        return new ElementiZahteva();
    }

    /**
     * Create an instance of {@link DostavljanjeKopijeDokumenta }
     * 
     */
    public DostavljanjeKopijeDokumenta createDostavljanjeKopijeDokumenta() {
        return new DostavljanjeKopijeDokumenta();
    }

    /**
     * Create an instance of {@link InformacijeOTraziocu }
     * 
     */
    public InformacijeOTraziocu createInformacijeOTraziocu() {
        return new InformacijeOTraziocu();
    }

    /**
     * Create an instance of {@link Adresa }
     * 
     */
    public Adresa createAdresa() {
        return new Adresa();
    }

    /**
     * Create an instance of {@link Zahtevi }
     * 
     */
    public Zahtevi createZahtevi() {
        return new Zahtevi();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "elektronskom_postom")
    public JAXBElement<String> createElektronskomPostom(String value) {
        return new JAXBElement<String>(_ElektronskomPostom_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "postom")
    public JAXBElement<String> createPostom(String value) {
        return new JAXBElement<String>(_Postom_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "drugi_nacin")
    public JAXBElement<String> createDrugiNacin(String value) {
        return new JAXBElement<String>(_DrugiNacin_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "stav")
    public JAXBElement<Byte> createStav(Byte value) {
        return new JAXBElement<Byte>(_Stav_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "kopija_dokumenta")
    public JAXBElement<String> createKopijaDokumenta(String value) {
        return new JAXBElement<String>(_KopijaDokumenta_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "broj")
    public JAXBElement<Short> createBroj(Short value) {
        return new JAXBElement<Short>(_Broj_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "pravno_lice")
    public JAXBElement<String> createPravnoLice(String value) {
        return new JAXBElement<String>(_PravnoLice_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "zakon")
    public JAXBElement<String> createZakon(String value) {
        return new JAXBElement<String>(_Zakon_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Byte }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "broj_clana")
    public JAXBElement<Byte> createBrojClana(Byte value) {
        return new JAXBElement<Byte>(_BrojClana_QNAME, Byte.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "uvid_u_dokument")
    public JAXBElement<String> createUvidUDokument(String value) {
        return new JAXBElement<String>(_UvidUDokument_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "drugi_podaci_za_kontakt")
    public JAXBElement<Integer> createDrugiPodaciZaKontakt(Integer value) {
        return new JAXBElement<Integer>(_DrugiPodaciZaKontakt_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Short }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "grad")
    public JAXBElement<Short> createGrad(Short value) {
        return new JAXBElement<Short>(_Grad_QNAME, Short.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "ulica")
    public JAXBElement<String> createUlica(String value) {
        return new JAXBElement<String>(_Ulica_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "posedovanje_informacije")
    public JAXBElement<String> createPosedovanjeInformacije(String value) {
        return new JAXBElement<String>(_PosedovanjeInformacije_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "faksom")
    public JAXBElement<String> createFaksom(String value) {
        return new JAXBElement<String>(_Faksom_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.euprava.poverenik.gov.rs/request", name = "informacije_na_koje_se_odnosi_zahtev")
    public JAXBElement<String> createInformacijeNaKojeSeOdnosiZahtev(String value) {
        return new JAXBElement<String>(_InformacijeNaKojeSeOdnosiZahtev_QNAME, String.class, null, value);
    }

}
