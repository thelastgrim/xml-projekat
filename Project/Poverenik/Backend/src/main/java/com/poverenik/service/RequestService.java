package com.poverenik.service;

import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Comparator;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.UnmarshallerHandler;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.exist.source.StringSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.poverenik.model.requests.Zahtev;
import com.poverenik.repos.RequestRepo;
import com.poverenik.util.Constants;
import com.poverenik.util.Person;



@Service
public class RequestService {
	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/request";
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private SchemaFactory schemaFactory;
	private Schema schema;
	
	
	private String newId;
	
	@Autowired
	private RequestRepo requestRepo;
	
	public List<Zahtev> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		return requestRepo.findAllByUserEmail(email);
	}
	
	public List<Zahtev> getAll() throws XMLDBException, JAXBException{
		return requestRepo.getAll();
	}

	public void create(String zahtev) throws Exception {
		getNextId();
		
		context = JAXBContext.newInstance("com.poverenik.model.requests");
		unmarshaller = context.createUnmarshaller();
		schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		schema = schemaFactory.newSchema(new File(Constants.ZAHTEV_XSD_LOCATION));
		unmarshaller.setSchema(schema);
		

		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(zahtev));
			
		JAXBElement<Zahtev> zahtevObj =  unmarshaller.unmarshal(reader, Zahtev.class);
		
		Zahtev z = zahtevObj.getValue();
		z.setAbout(TARGET_NAMESPACE+"/"+newId);
		z.setVocab("http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/");
		z.setId(newId);
		z.setPodnosilac(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		
		Marshaller marshaller = context.createMarshaller();
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(z, stringWriter);
		
		String tst = stringWriter.toString().substring(55); //smacinje <?xml.... sa pocetka , damo ze da se upise

		
		requestRepo.create(z, tst);
			
	}
	
	private void getNextId() throws XMLDBException, JAXBException {
		List<Zahtev> zahtevi = getAll();
		zahtevi.sort(Comparator.comparing(Zahtev::getId));
		newId = "ZA_"+ (Integer.parseInt(zahtevi.get(zahtevi.size()-1).getId().split("_")[1])+1);
	}
	
}
