//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2021.02.05 at 01:58:54 PM CET 
//


package com.poverenik.model.decision;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://www.euprava.poverenik.gov.rs/decision}tekst"/>
 *         &lt;element ref="{http://www.euprava.poverenik.gov.rs/decision}primalac_resenja"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "tekst",
    "primalacResenja"
})
@XmlRootElement(name = "tekst_resenja")
public class TekstResenja {

    @XmlElement(required = true)
    protected String tekst;
    @XmlElement(name = "primalac_resenja", required = true)
    protected PrimalacResenja primalacResenja;

    /**
     * Gets the value of the tekst property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTekst() {
        return tekst;
    }

    /**
     * Sets the value of the tekst property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTekst(String value) {
        this.tekst = value;
    }

    /**
     * Gets the value of the primalacResenja property.
     * 
     * @return
     *     possible object is
     *     {@link PrimalacResenja }
     *     
     */
    public PrimalacResenja getPrimalacResenja() {
        return primalacResenja;
    }

    /**
     * Sets the value of the primalacResenja property.
     * 
     * @param value
     *     allowed object is
     *     {@link PrimalacResenja }
     *     
     */
    public void setPrimalacResenja(PrimalacResenja value) {
        this.primalacResenja = value;
    }

}
