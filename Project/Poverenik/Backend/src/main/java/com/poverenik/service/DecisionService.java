package com.poverenik.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;
import org.xmldb.api.base.XMLDBException;

import com.poverenik.database.FusekiManager;
import com.poverenik.model.appealDecision.Zalba;
import com.poverenik.model.decision.Resenje;
import com.poverenik.repos.DecisionRepo;
import com.poverenik.util.AppealSilenceTransform;
import com.poverenik.util.Constants;
import com.poverenik.util.DecisionTransform;
import com.poverenik.util.MetadataExtractor;
import com.poverenik.util.Person;

@Service
public class DecisionService {

	private final String TARGET_NAMESPACE = "http://www.euprava.poverenik.gov.rs/";
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private SchemaFactory schemaFactory;
	private Schema schema;

	@Autowired
	FusekiManager fusekiManager;
	
	private String newId;
	
	@Autowired
	private DecisionRepo decisionRepo;
	
	public List<Resenje> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		return decisionRepo.findAllByUserEmail(email);
	}

	public List<Resenje> getAll() throws XMLDBException, JAXBException {
		return decisionRepo.getAll();
	}
	
	
	public String findById(String id) throws XMLDBException {
		return decisionRepo.findById(id);
	}
	
	public File exportAsPDF(String id) throws XMLDBException, SAXException, IOException, TransformerException {
		String resenje = findById(id);
		
		DecisionTransform ast = new DecisionTransform();
		String file = String.format("src/main/resources/static/documents/resenje-%s.pdf", id);
		ast.makePDF(this.removeNamespace(resenje), file);
		
		return new File(file);
	}
	
	public File exportAsHTML(String id) throws XMLDBException, ParserConfigurationException, SAXException, IOException, TransformerException {
		String zalba = findById(id);
		
		DecisionTransform ast = new DecisionTransform();
		String file = String.format("src/main/resources/static/documents/resenje.html", id);
		ast.makeHTML(this.removeNamespace(zalba), file);
		
		return new File(file);
	}
	
	public File exportMetadataAsJSON(String id) throws IOException {
		
		String file = String.format("src/main/resources/static/documents/resenje-%s.JSON", id);
		fusekiManager.makeJSONForDecision(id, file);
		
		return new File(file);
	}
	
	public File exportMetadataAsRDF(String id) throws SAXException, IOException, XMLDBException {
		String file = String.format("src/main/resources/static/documents/resenje-%s.RDF", id);
		String fileXml = String.format("src/main/resources/static/documents/resenje-%s.xml", id);
		MetadataExtractor metadataExtractor = new MetadataExtractor();
		
		String zalba = removeNamespace(findById(id));
		
		
		
		String output = zalba.substring(0, 46) + 
				" xmlns:pred=\"http://www.euprava.poverenik.gov.rs/rdf/examples/predicate/\" "+
				zalba.substring(47);

	
		FileWriter fw = new FileWriter(fileXml);
		fw.write(output);
		fw.close();
		
		try {
			metadataExtractor.extractMetadata(new FileInputStream(new File(fileXml)),
					new FileOutputStream(new File(file)));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return new File(file);
	}
	
	
	public String removeNamespace(String xml) {
		 try{
	       TransformerFactory factory = TransformerFactory.newInstance();
	        Source xslt = new StreamSource(new File(Constants.NS_REMOVER));
	        Transformer transformer = factory.newTransformer(xslt);
	        StringReader r = new StringReader(xml);
	        StreamSource text = new StreamSource(r);
	        
	        StringWriter writer = new StringWriter();
	        
	        
	        transformer.transform(text, new StreamResult(writer));
	        System.out.println("Done");
	        return writer.toString();
	        } catch (TransformerConfigurationException e) {
	           return "";
	        } catch (TransformerException e) {
	        	return "";
	        }
	
	}

	public void create(String resenje) throws FactoryConfigurationError, Exception {
		getNextId();
		resenje = resenje.replace("xml:space='preserve'", "");
		
		context = JAXBContext.newInstance("com.poverenik.model.decision");
		unmarshaller = context.createUnmarshaller();
		schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		schema = schemaFactory.newSchema(new File(Constants.RESENJE_XSD_LOCATION));
		unmarshaller.setSchema(schema);
		
		XMLStreamReader reader = XMLInputFactory.newInstance().createXMLStreamReader(new StringReader(resenje));
		
		JAXBElement<Resenje> resenjeObj =  unmarshaller.unmarshal(reader, Resenje.class);
		
		Resenje r = resenjeObj.getValue();
		r.setAbout(TARGET_NAMESPACE+"rdf/examples/decision/"+newId);
		r.setVocab("http://www.euprava.poverenik.gov.rs/predicate/");
		r.getBrojResenja().setValue(newId);
		r.setPodnosilac(((Person) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getEmail());
		Date date = new Date();
		String str = new SimpleDateFormat("dd.MM.yyyy").format(date);
		r.setDatum(str);
		Marshaller marshaller = context.createMarshaller();
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(r, stringWriter);
		
		String tst = stringWriter.toString().substring(55); //smacinje <?xml.... sa pocetka , damo ze da se upise
		
		decisionRepo.create(r, tst);
	}
	
	private void getNextId() throws XMLDBException, JAXBException {
		List<Resenje> resenja = getAll();
		
		Collections.sort(resenja, new Comparator<Resenje>() {
			@Override
			public int compare(Resenje r1, Resenje r2) {
				
			return r1.getBrojResenja().getValue().split("-")[1]
					.compareTo(r2.getBrojResenja().getValue().split("-")[1]);
				
			}
			});
	
		newId = "071-"+ (Integer.parseInt(resenja.get(resenja.size()-1).getBrojResenja()
				.getValue().split("-")[1])+1)+"-1114-2020-03";
	}

	public List<Resenje> findAllByContains(String text) throws XMLDBException, JAXBException {
		
		return decisionRepo.findAllByContains(text);
	}
}
