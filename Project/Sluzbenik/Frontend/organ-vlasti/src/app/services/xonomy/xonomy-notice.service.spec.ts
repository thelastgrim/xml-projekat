/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { XonomyNoticeService } from './xonomy-notice.service';

describe('Service: XonomyNotice', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [XonomyNoticeService]
    });
  });

  it('should ...', inject([XonomyNoticeService], (service: XonomyNoticeService) => {
    expect(service).toBeTruthy();
  }));
});
