import { Injectable } from '@angular/core';

declare const Xonomy : any;

@Injectable({
  providedIn: 'root'
})
export class XonomyNoticeService {

constructor() { }

public noticeSpecification = {
  validate: function (jsElement) {
    //Validate the element:
    let elementSpec = this.elements[jsElement.name];
    if (elementSpec.validate) elementSpec.validate(jsElement);
    //Cycle through the element's attributes:
    for (let i = 0; i < jsElement.attributes.length; i++) {
      let jsAttribute = jsElement.attributes[i];
      let attributeSpec = elementSpec.attributes[jsAttribute.name];
      if (attributeSpec.validate) attributeSpec.validate(jsAttribute);
    }
    //Cycle through the element's children:
    for (let i = 0; i < jsElement.children.length; i++) {
      let jsChild = jsElement.children[i];
      if (jsChild.type == "element") { //if element
        this.validate(jsChild); //recursion
      }
    }
  },
  elements: {
    obavestenje : {
      hasText: false,
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      menu: [],
      attributes: {
        "xmlns": {
          isInvisible: true,
        },
        "xmlns:pred": {
          isInvisible: true,
        },
        "idZahteva" : {
          isReadOnly : true,
        }
      }
    },

    //podaci o organu
    podaci_o_primaocu: {
      hasText : false,
      isReadOnly : true,

    },
    naziv : {
      hasText: true,
      oneliner : true,
      isReadOnly: true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    sediste : {
      hasText: true,
      oneliner : true,
      isReadOnly: true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    //Podaci o zakonu
    datum_podnosenja : {
      isInvisible : true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    broj_predmeta : {
      isInvisible : true
    },
    podaci_o_podnosiocu : {
      hasText: false,
      isReadOnly : true
    },
    ime_podnosioca : {
      hasText: true,
      isReadOnly : true,
      oneliner : true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },

    //Elementi zahteva
    prezime_podnosioca : {
      hasText: true,
      isReadOnly : true,
      oneliner : true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    naziv_podnosioca : {
      hasText: true,
      isReadOnly : true,
      oneliner : true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }

    },
    adresa_podnosioca : {
      hasText: true,
      isReadOnly : true,
      oneliner : true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    trazena_informacija : {
      hasText: false,
      isReadOnly : true,
    },
    datum_zahteva : {
      hasText: true,
      oneliner : true,
      isReadOnly: true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    opis_trazene_informacije : {
      hasText: true,
      oneliner : true,
      isReadOnly: true,
      attributes: {
        "property": {
          isInvisible: true,
        }
      }
    },
    datum_uvida : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      oneliner : true,
      asker : Xonomy.askString,
    },
    od_vreme : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      oneliner : true,
      asker : Xonomy.askString,
    },
    do_vreme : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      oneliner : true,
      asker : Xonomy.askString,
    },
    grad : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      oneliner : true,
      asker : Xonomy.askString,
    },
    ulica : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      oneliner : true,
      asker : Xonomy.askString,
    },
    broj_ulice : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      oneliner : true,
      asker : Xonomy.askString,
    },
    broj_kancelarije : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Ovaj element ne sme biti prazan!"
            }
          );
        }
      },
      hasText: true,
      oneliner : true,
      asker : Xonomy.askString,

    },
    troskovi : {
      isInvisible : true,
      hasText : false,

    },
    format : {
      isInvisible : true,
      hasText : true,

    },
    uplata : {
      hasText : false,

    },
    iznos_za_uplatu : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Unesite ime firme."
            }
          );
        }
      },
      hasText: true,
      asker: Xonomy.askString,
      oneliner: true,
    },
    racun : {
      validate: function (jsElement) {
        if (jsElement.getText() == "") {
          Xonomy.warnings.push({
              htmlID: jsElement.htmlID,
              text: "Unesite broj računa"
            }
          );
        }
      },
      hasText: true,
      asker: Xonomy.askString,
      attributes: {
        "broj": {
          hasText: true,
          asker : Xonomy.askString,
        },
        "model": {
          hasText: true,
          asker : Xonomy.askString,
        }
      }
    },

    dostavljeno : {
      isInvisible : true,
    },
    potpis_ovlascenog_lica : {
      isInvisible : true,
    }
  }
}


}
