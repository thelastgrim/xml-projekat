import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NoticeService {

  private readonly path = 'http://localhost:8082/notices';

constructor(private http: HttpClient) { }

getAllNotices(){
  const headers = new HttpHeaders({
    'Content-Type' : 'application/xml',
    'Accept' : 'application/xml'
  });

  return this.http.get(this.path, {headers:headers, responseType:'text'});

}

getAllNoticesForCitizen(){
  const headers = new HttpHeaders({
    'Content-Type' : 'application/xml',
    'Accept' : 'application/xml'
  });

  return this.http.get(`${this.path}/user`, {headers:headers, responseType:'text'});

}

createNotice(notice : any){
  const headers = new HttpHeaders({
    'Content-Type' : 'application/xml',
    'Accept' : 'application/xml'
  });

  return this.http.put(this.path, notice, {headers:headers, responseType:'text'});
}

export(id: string, type :string){

  return  this.http.get(`${this.path}/export/${id}/${type}`, {responseType: 'arraybuffer' as 'text' });
}

}
