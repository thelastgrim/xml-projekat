/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Display_searchComponent } from './display_search.component';

describe('Display_searchComponent', () => {
  let component: Display_searchComponent;
  let fixture: ComponentFixture<Display_searchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Display_searchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Display_searchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
