import { Component, Input, OnChanges, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth/auth.service';
import { NotificationService } from 'src/app/services/notification.service';
import { RequestService } from 'src/app/services/request/request.service';
import { CreateNoticeComponent } from '../create-notice/create-notice.component';
import * as txml from 'txml';
import * as js2xml from 'js2xmlparser';

@Component({
  selector: 'app-display_search',
  templateUrl: './display_search.component.html',
  styleUrls: ['./display_search.component.scss']
})
export class Display_searchComponent implements OnChanges {

  currentUser : any;
  declining: boolean = false;
  @Input() dataSourceRequests: MatTableDataSource<any>;
  @Input() requests : any;
  displayedColumns: string[] = [
    'brojZahtjeva',
    'trazilac',
    'mjesto',
    'datumPodnosenja',
    'organ',
    'odobren',
    'odbijanje',
    'obavjestenje',
    'preuzimanje'
  ];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private dialog: MatDialog, private notification : NotificationService, private requestService : RequestService, private authService : AuthService, private router : Router) {
    this.currentUser = authService.getCurrentUser();
  }

  ngOnChanges() {
    this.dataSourceRequests.sort = this.sort;
  }

  decline(brojZahtjeva : string){
    console.log(brojZahtjeva);

    let _declinedReq : any = {
      "@" : {
        id : brojZahtjeva,
      }
    }

    let declinedReq = js2xml.parse('Request', _declinedReq);
    if(confirm("Želite li odbiti zahtev?")){
      this.declining = true;
    this.requestService.declineRequest(declinedReq).subscribe(() => {
      console.log("Success");
      this.notification.openSnackBar("Zahtev je odbijen!");
      this.declining = false;
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/home/requests']);
    });
    }
  }

  createNotice(element : any){
    console.log(this.requests);
    let request : any;
    if(this.requests.List.item.length > 1){
      this.requests.List.item.forEach(req => {
        if(req.id === element)
            request = req;
      });
    }else if(this.requests.List.item.id === element){
      request = this.requests.List.item;

    }
    const dialogRef = this.dialog.open(CreateNoticeComponent, {
      data: {
         request
      },
      maxHeight: window.innerHeight + 'px',
    });
    dialogRef.afterClosed().subscribe((result) => {
      console.log(result);
      //this.notification.openSnackBar("Obaveštenje je dodano!");
      this.router.navigate(['/home/notices']);
    });
  }

  exportPDF(elementId : any){
    this.requestService.export(elementId, "PDF").subscribe((response) =>{
      console.log(response);
      let file = new Blob([response], { type: 'application/pdf' });
      var fileURL = URL.createObjectURL(file);
      let a = document.createElement('a');
      document.body.appendChild(a);
      a.setAttribute('style', 'display: none');
      a.href = fileURL;
      a.download = `Zahtev_${elementId}.pdf`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),(error) => this.notification.openSnackBar("Desila se greška!");
    () => this.notification.openSnackBar("Uspješno ste skinuli dokument u PDF formatu!");

  }

  exportRDF(elementId : any){
  this.requestService.export(elementId, "RDF").subscribe((response) => {
    let file = new Blob([response], { type: 'application/RDF' });

    var fileURL = URL.createObjectURL(file);

    let a = document.createElement("a");

    document.body.appendChild(a);

    a.setAttribute("style", "display: none");

    a.href = fileURL;

    a.download = `Zahtev_${elementId}.rdf`;
    a.click();
    window.URL.revokeObjectURL(fileURL);
    a.remove();
  }),
    (error) => this.notification.openSnackBar("Desila se greška!");
    () => this.notification.openSnackBar("Uspješno ste izvezli dokument u RDF formatu!");
  }

  exportJSON(elementId : any){

    this.requestService.export(elementId, "RDF").subscribe((response) => {
      let file = new Blob([response], { type: 'application/JSON' });

      var fileURL = URL.createObjectURL(file);

      let a = document.createElement("a");

      document.body.appendChild(a);

      a.setAttribute("style", "display: none");

      a.href = fileURL;

      a.download = `Zahtev_${elementId}.json`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notification.openSnackBar("Desila se greška!");
      () => this.notification.openSnackBar("Uspješno ste izvezli dokument u JSON formatu!");
  }

  exportXHTML(elementId : any){
    this.requestService.export(elementId, "HTML").subscribe((response) => {
      let file = new Blob([response], { type: 'text/html' });

      var fileURL = URL.createObjectURL(file);

      let a = document.createElement("a");

      document.body.appendChild(a);

      a.setAttribute("style", "display: none");

      a.href = fileURL;

      a.download = `Zahtev_${elementId}.html`;
      a.click();
      window.URL.revokeObjectURL(fileURL);
      a.remove();
    }),
      (error) => this.notification.openSnackBar("Desila se greška!");
      () => this.notification.openSnackBar("Uspješno ste skinuli dokument u HTML formatu!");
  }

}
