import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateNoticeComponent } from './components/create-notice/create-notice.component';
import { CreateRequestComponent } from './components/create-request/create-request.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LogInComponent } from './components/log-in/log-in.component';
import { NoticesComponent } from './components/notices/notices.component';
import { OptionsComponent } from './components/options/options.component';
import { RegisterComponent } from './components/register/register.component';
import { RequestsComponent } from './components/requests/requests.component';
import { AuthGuard } from './guards/auth.guard';
import { LoggedInGuard } from './guards/logged-in.guard';


const routes: Routes = [
  { path: '',   redirectTo: 'login', pathMatch: 'full' },
  {path: 'register', component: RegisterComponent, canActivate: [LoggedInGuard]},
  {path: 'login', component: LogInComponent, canActivate: [LoggedInGuard]},
  {path: 'home', component : OptionsComponent, canActivate: [AuthGuard],
  children: [
    { path: '',   redirectTo: 'dashboard', pathMatch: 'full' },
    {
      path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'requests',
        component: RequestsComponent
    },
    {
      path: 'requests/create',
      component: CreateRequestComponent
  },
    {
      path: 'notices',
      component: NoticesComponent
    },
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
