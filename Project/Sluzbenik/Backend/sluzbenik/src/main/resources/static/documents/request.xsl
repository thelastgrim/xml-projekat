<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
	<xsl:template match="/">
		<html>
			<head>
				<style>
					body {
					background-color: grey;
					}
					.c1 {
					padding-left: 50pt;
					margin: 0 auto;
					margin-top: 20pt;
					margin-bottom: 20pt;
					background-color: white;
					padding-right: 50pt;
					padding-top: 60pt;
					padding-bottom: 60pt;
					box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
					width: 40%;
					position: sticky;
					top: 25pt;
					}

					.c6 {
					font-family: "Times New Roman";
					font-size: 14pt;
					text-align: center;
					margin-top:48pt;
					}

					.c8 {
					font-family: "Times New Roman";
					margin-top: 10pt;
					text-align:
					center;
					}

					.c9 {
					text-align: center;
					font-size: 12pt;
					font-family:
					"Times New Roman";
					margin-top:-10pt;
					}

					.c20 {
					font-family:"Times New Roman";
					font-size:12pt;
					text-align:justify;
					text-indent:4em;
					margin-top:28pt;
					}

					.c24 {
					text-align:right;
					font-size: 10pt;
					font-family: "Times New Roman";
					margin-top:-10pt;
					}

					.c25 {
					font-family:"Times New Roman";
					font-size:10pt;
					text-align:right;
					}

					.c27 {
					font-family:"Times New Roman";
					font-size:10pt;
					text-align:left;
					margin-top:-50pt;
					}

					.c29 {
					font-family:"Times New Roman";
					font-size:10pt;
					text-align:left;
					margin-top:20pt;
					}

					.c37 {
					font-family: "Times New Roman";
					font-size: 14pt;
					text-align: center;
					margin-top: -10pt;
					}
					.c38 {
					font-family: "Times New Roman";
					font-size:9pt;
					margin-top: -10pt;
					text-align:justify;
					}

					.c39
					{
					font-family: "Times New Roman";
					font-size: 9pt;
					margin-top: 40pt;
					text-align:justify;
					}

					.c40{
					margin-top:-10pt;
					font-family:"Times New Roman";
					font-size:9pt;
					text-align:justify;
					}

					.c41{
					margin-top:-10pt;
					font-family:"Times New Roman";
					font-size:12pt;
					text-align:justify;
					text-indent:4em;
					}

					.c42{
					font-family:"Times New Roman";
					font-size:12pt;
					text-align:justify;
					text-indent:4em;
					}

					.c43{
					font-family:"Times New Roman";
					font-size:12pt;
					text-indent:4em;
					margin-top:-10pt;
					}

					.c44{
					font-family:"Times New Roman";
					font-size:12pt;
					text-indent:8em;
					margin-top:-10pt;
					}

					.c45{
					font-family:"Times New Roman";
					font-size:12pt;
					padding-left: 60px;
					margin-top:12pt;
					}

					.c46{
					font-family:"Times New Roman";
					font-size:10pt;
					text-align:right;
					margin-top:27pt;
					}
				</style>
			</head>
			<body>
				<div class="c1">
					<p class="c8">
						<u><xsl:value-of select="/zahtev/podaci_o_organu/naziv_organa"></xsl:value-of>
  						,
  						<xsl:value-of  select="/zahtev/podaci_o_organu/sediste_organa"></xsl:value-of>
						</u>
					</p>
					<p class="c9">назив и седиште органа коме се захтев упућује</p>
					<p class="c6">
						<strong>З А Х Т Е В</strong>
					</p>
					<p class="c37">
						<strong>за приступ информацији од јавног значаја</strong>
					</p>
					<p class="c20">
						На основу члана 15. ст. 1. Закона о слободном приступу
						информацијама од јавног значаја („Службени гласник РС“, бр.
						120/04, 54/07, 104/09 и 36/10), од горе наведеног органа
						захтевам:*
					</p>
					<p class="c45">
					    <xsl:if
										test="zahtev/elementiZahteva/posedovanje_informacije = 'true'">
										<span font-size="12pt" font-family="MS Gothic">
											☑ обавештење да ли поседује тражену информацију;
										</span>
									</xsl:if>
									<xsl:if test="zahtev/elementiZahteva/posedovanje_informacije = 'false'">
										<span font-size="12pt" font-family="MS Gothic">
											☐ обавештење да ли поседује тражену информацију;
										</span>
									</xsl:if>
								</p>
								<p>
									<xsl:if
										test="zahtev/elementiZahteva/uvid_u_dokument = 'true'">
										<span font-size="12pt" font-family="MS Gothic">
											☑ увид у документ који садржи тражену информацију;
										</span>
									</xsl:if>
									<xsl:if
										test="zahtev/elementiZahteva/uvid_u_dokument = 'false'">
										<span font-size="12pt" font-family="MS Gothic">
											☐ увид у документ који садржи тражену информацију;
										</span>
									</xsl:if>
</p><p>
									<xsl:if
										test="zahtev/elementiZahteva/kopija_dokumenta = 'true'">
										<span font-size="12pt" font-family="MS Gothic">
											☑ копију документа који садржи тражену информацију;
										</span>
									</xsl:if>
									<xsl:if
										test="zahtev/elementiZahteva/kopija_dokumenta = 'false'">
										<span font-size="12pt" font-family="MS Gothic">
											☐  копију документа који садржи тражену информацију;
										</span>
									</xsl:if>
					   </p>
				
					<p class="c42">
						Овај захтев се
						односи на следеће информације:
					</p>
					<p class="c41">
						<u><xsl:value-of
						    select="/zahtev/informacije_na_koje_se_odnosi_zahtev"></xsl:value-of></u>
					</p>
					<p class="c40">(навести што прецизнији опис
						информације која се
						тражи
						као
						и друге податке који олакшавају
						проналажење тражене
						информације)</p>
					<p class="c46">
						<u><xsl:value-of
						    select="/zahtev/informacije_o_traziocu/trazioc/fizicko_lice/ime"></xsl:value-of></u>
					</p>
					<p class="c46">
						<u><xsl:value-of
						    select="/zahtev/informacije_o_traziocu/trazioc/fizicko_lice/prezime"></xsl:value-of></u>
					</p>
					<p class="c24">Тражилац информације/Име и презиме</p>
					<p class="c25">
					    <u><xsl:value-of
					        select="concat(/zahtev/informacije_o_traziocu/adresa/ulica, ' ')"></xsl:value-of>
					    <xsl:value-of
					        select="/zahtev/informacije_o_traziocu/adresa/broj"></xsl:value-of>
					    ,
					    <xsl:value-of
					        select="/zahtev/informacije_o_traziocu/adresa/mesto"></xsl:value-of></u>
					</p>
					<p class="c24">адресa</p>
					<p class="c25">
						<u><xsl:value-of
						    select="/zahtev/informacije_o_traziocu/drugi_podaci_za_kontakt"></xsl:value-of></u>
					</p>
					<p class="c24">други подаци за контакт</p>
					<p class="c27">
						У
						<u><xsl:value-of
						    select="/zahtev/@mesto"></xsl:value-of></u>
						,

					</p>
					<p class="c29">
						дана
							<u><xsl:value-of
							    select="/zahtev/@datum" /></u>
						. године
					</p>
					<p class="c39">__________________________________________</p>
					<p class="c38">* У кућици означити која законска права на приступ
						информацијама желите да остварите.</p>
					<p class="c38">** У кућици означити начин достављања копије
						докумената.</p>
					<p class="c38">*** Када захтевате други начин достављања обавезно
						уписати који начин достављања захтевате.</p>
				</div>
			</body>
		</html>
	</xsl:template>
</xsl:stylesheet>