package com.sluzbenik.repos;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.exist.xupdate.XUpdateProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XMLResource;

import com.sluzbenik.database.ExistManager;
import com.sluzbenik.database.FusekiManager;
import com.sluzbenik.model.request.Zahtev;
import com.sluzbenik.util.Constants;

@Repository
public class RequestRepo {
	
	@Autowired
	private ExistManager existManager;
	@Autowired
	private FusekiManager fusekiManager;
	
	private String query;
	private JAXBContext context;
	private Unmarshaller unmarshaller;
	private ResourceIterator it;
	List<Zahtev> requests;
	Zahtev request;
	private final String TARGET_NAMESPACE = "http://www.euprava.sluzbenik.gov.rs/request";
	private final String APPEND = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:append select=\"%1$s\" child=\"last()\">%2$s</xu:append>"
			+ "</xu:modifications>";
	
	private final String UPDATE = "<xu:modifications version=\"1.0\" xmlns:xu=\"" + XUpdateProcessor.XUPDATE_NS
			+ "\" xmlns=\"" + TARGET_NAMESPACE + "\">" + "<xu:update select=\"%1$s\">%2$s</xu:update>"
			+ "</xu:modifications>";
	
	
	public List<Zahtev> findAll() throws XMLDBException, JAXBException{
		
//		query = String.format("/zahtevi/zahtev[@podnosilac='%s']", email);
		query = String.format("/zahtevi/zahtev");

									
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zahtev>();
			it = set.getIterator();
			context = JAXBContext.newInstance("com.sluzbenik.model.request");
			unmarshaller = context.createUnmarshaller();
			Zahtev z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zahtev) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				System.out.println(z.getId());
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
		
	}
	
	public List<Zahtev> findAllByUserEmail(String email) throws XMLDBException, JAXBException{
		
		query = String.format("/zahtevi/zahtev[@podnosilac='%s']", email);
	
					
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zahtev>();
			context = JAXBContext.newInstance("com.sluzbenik.model.request");
			unmarshaller = context.createUnmarshaller();
			Zahtev z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zahtev) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
		
	}
	
	public List<Zahtev> findAllByContainsId(String id) throws XMLDBException, JAXBException{
		
		query = String.format("/zahtevi/zahtev[contains(@id, '%s')]", id);
	
					
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zahtev>();
			context = JAXBContext.newInstance("com.sluzbenik.model.request");
			unmarshaller = context.createUnmarshaller();
			Zahtev z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zahtev) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				requests.add(z);
			}
			 
			return requests;	
		}
		else {
			return null;
		}
		
	}
	
	public List<Zahtev> findAllByContains(String text) throws XMLDBException, JAXBException{
		query = String.format("/zahtevi/zahtev[podaci_o_organu/naziv_organa[contains(., '%1$s')] or "
				                             + "podaci_o_organu/sediste_organa[contains(., '%1$s')] or "
				                             +  "informacije_na_koje_se_odnosi_zahtev[contains(., '%1$s')] or "
				                             +  "informacije_o_traziocu/adresa/grad[contains(., '%1$s')] or "
				                             +  "informacije_o_traziocu/adresa/ulica[contains(., '%1$s')] or "
				                             +  "informacije_o_traziocu/adresa/broj[contains(., '%1$s')] or "
				                             +  "informacije_o_traziocu/drugi_podaci_za_kontakt[contains(., '%1$s')] or "
				                             +  "informacije_o_traziocu/trazioc/pravno_lice[contains(., '%1$s')] or "
				                             +  "informacije_o_traziocu/trazioc/fizicko_lice/ime[contains(., '%1$s')] or "
				                             +  "informacije_o_traziocu/trazioc/fizicko_lice/prezime[contains(., '%1$s')]]", text);
		
		
		System.out.println(text);
		System.out.println(query);
				
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
	
		if(set.getSize() != 0) {
			
			requests = new ArrayList<Zahtev>();
			context = JAXBContext.newInstance("com.sluzbenik.model.request");
			unmarshaller = context.createUnmarshaller();
			Zahtev z = null;
			for (int i = 0; i < set.getSize(); i++) {
				z = (Zahtev) unmarshaller.unmarshal(((XMLResource)set.getResource(i)).getContentAsDOM());
				
				requests.add(z);
			}
			
			return requests;	
		}
		else {
			return null;
		}
			
	}
	
	public Zahtev findById(String id) throws XMLDBException, JAXBException{
		
		query = String.format("/zahtevi/zahtev[@id='%s']", id);
	
					
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() != 0) {
			
			request = new Zahtev();
			context = JAXBContext.newInstance("com.sluzbenik.model.request");
			unmarshaller = context.createUnmarshaller();
			request = null;
			request = (Zahtev) unmarshaller.unmarshal(((XMLResource)set.getResource(0)).getContentAsDOM());
			 
			return request;	
		}
		else {
			return null;
		}
		
	}
	
	public void create(Zahtev zahtevObj, String zahtev) throws Exception {
		existManager.add(Constants.COLLECTION_URI,
						 Constants.ZAHTEVCIR_ID,
						 "/zahtevi",
						 zahtev,
						 APPEND);
		fusekiManager.addRequest(zahtevObj);
		
	}
	
	public void update(Zahtev zahtevObj, String change, String root) throws Exception {
		
		existManager.update(Constants.COLLECTION_URI,
						 Constants.ZAHTEVCIR_ID,
						 root,
						 change,
						 UPDATE);
		
	}
	
	public String findByIdString(String id) throws XMLDBException {
		query = String.format("/zahtevi/zahtev[@id='%s']", id);
		
		ResourceSet set = this.existManager.retrieve(Constants.COLLECTION_URI, query, TARGET_NAMESPACE);
		
		if(set.getSize() !=0) {
			
			return set.getResource(0).getContent().toString();
		}else {
			return null;
		}
		
		
	}
	

}
