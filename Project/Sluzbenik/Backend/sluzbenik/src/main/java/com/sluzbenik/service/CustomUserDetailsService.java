package com.sluzbenik.service;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.xmldb.api.base.XMLDBException;

import com.sluzbenik.model.user.User;
import com.sluzbenik.repos.UserRepo;
import com.sluzbenik.util.Person;



// Ovaj servis je namerno izdvojen kao poseban u ovom primeru.
// U opstem slucaju UserServiceImpl klasa bi mogla da implementira UserDetailService interfejs.
@Service
public class CustomUserDetailsService implements UserDetailsService{
	
    @Autowired
    private UserRepo userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    // Funkcija koja na osnovu username-a iz baze vraca objekat User-a
    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        // ako se ne radi nasledjivanje, paziti gde sve treba da se proveri email
        Person person = new Person();
        User user = null;
		try {
			user = userRepository.findByEmail(email);
		} catch (JAXBException | XMLDBException e) {
			// TODO Auto-generated catch block
			System.out.println("greska");
			e.printStackTrace();
		}
        if (user == null) {
            throw new UsernameNotFoundException(String.format("No user found with username '%s'.", email));
        } else {
        	person.setEmail(user.getEmail());
        	person.setUsername(user.getEmail());
        	person.setPassword(user.getPassword());
        	System.out.println("persona napravljeno");
        	 //Set<Authority> grantedAuthoritySet = new HashSet<>();
    		// grantedAuthoritySet.add(new Authority("ROLE_CITIZEN"));
    		 //user.setAuthorities(grantedAuthoritySet);
            return person;
        }
    }

}
