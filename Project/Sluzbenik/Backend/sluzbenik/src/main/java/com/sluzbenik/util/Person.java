package com.sluzbenik.util;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class Person implements UserDetails{
	
	
	private String username;
	
	
	private String email;
	
	
	private String password;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		 Collection<GrantedAuthority> grantedAuthoritySet = new HashSet<>();
		 if(email.contains("euprava")) {
			 grantedAuthoritySet.add(new SimpleGrantedAuthority("ROLE_SERVANT"));
		 }else {
			 grantedAuthoritySet.add(new SimpleGrantedAuthority("ROLE_CITIZEN"));
		 }
		
		return grantedAuthoritySet;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}
