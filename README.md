Uputstvo za pokretanje

1. Pokrenuti Tomcat(sa fuseki i exist)
    <br /> Dodati u exist dve kolekcije: "documentsPoverenik" i "documentsSluzbenik"
    <br /> Dodatu u fuseki dva seta: "/documentsPoverenik" i "/documentsSluzbenik"
2. Clonovati projekat
    <br /> Importovati Project/Poverenik/Backend kao Existing Maven Project u eclipsu
    <br /> Selektovati sve biblioteke iz lib folder, desni klik, build path, add to buid path
    <br /> Desni klik na Poverenik projekat, Run As, Spring Boot App
    <br />
    <br /> Dodati Project/Poverenik/Fronted u Visual Studio Code
    <br /> U terminalu ukucati "npm i"
    <br /> Kad je gotov ukucati "ng serve"
    <br /> Na http://localhost:4200/ je applikacija

    <br />Sve isto i za Sluzbenik applikaciju 

    <br />
    <br />Videos:
    <br />https://easyupload.io/aryatq
    <br />https://easyupload.io/yl17oh
        
