
package com.team.xml.model;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ZahtevPodaciOOrganu_QNAME = new QName("", "podaci_o_organu");
    private final static QName _ZahtevZahtevNaOsnovu_QNAME = new QName("", "zahtev_na_osnovu");
    private final static QName _ZahtevMestoIDatum_QNAME = new QName("", "mesto_i_datum");
    private final static QName _ZahtevElementiZahteva_QNAME = new QName("", "elementiZahteva");
    private final static QName _ZahtevInformacijeNaKojeSeOdnosiZahtev_QNAME = new QName("", "informacije_na_koje_se_odnosi_zahtev");
    private final static QName _ZahtevInformacijeOTraziocu_QNAME = new QName("", "informacije_o_traziocu");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Zahtev }
     * 
     */
    public Zahtev createZahtev() {
        return new Zahtev();
    }

    /**
     * Create an instance of {@link Zahtev.InformacijeOTraziocu }
     * 
     */
    public Zahtev.InformacijeOTraziocu createZahtevInformacijeOTraziocu() {
        return new Zahtev.InformacijeOTraziocu();
    }

    /**
     * Create an instance of {@link Zahtev.InformacijeOTraziocu.Trazioc }
     * 
     */
    public Zahtev.InformacijeOTraziocu.Trazioc createZahtevInformacijeOTraziocuTrazioc() {
        return new Zahtev.InformacijeOTraziocu.Trazioc();
    }

    /**
     * Create an instance of {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice }
     * 
     */
    public Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice createZahtevInformacijeOTraziocuTraziocFizickoLice() {
        return new Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice();
    }

    /**
     * Create an instance of {@link Zahtev.ElementiZahteva }
     * 
     */
    public Zahtev.ElementiZahteva createZahtevElementiZahteva() {
        return new Zahtev.ElementiZahteva();
    }

    /**
     * Create an instance of {@link Zahtev.PodaciOOrganu }
     * 
     */
    public Zahtev.PodaciOOrganu createZahtevPodaciOOrganu() {
        return new Zahtev.PodaciOOrganu();
    }

    /**
     * Create an instance of {@link Zahtev.ZahtevNaOsnovu }
     * 
     */
    public Zahtev.ZahtevNaOsnovu createZahtevZahtevNaOsnovu() {
        return new Zahtev.ZahtevNaOsnovu();
    }

    /**
     * Create an instance of {@link Zahtev.MestoIDatum }
     * 
     */
    public Zahtev.MestoIDatum createZahtevMestoIDatum() {
        return new Zahtev.MestoIDatum();
    }

    /**
     * Create an instance of {@link Zahtev.InformacijeOTraziocu.Adresa }
     * 
     */
    public Zahtev.InformacijeOTraziocu.Adresa createZahtevInformacijeOTraziocuAdresa() {
        return new Zahtev.InformacijeOTraziocu.Adresa();
    }

    /**
     * Create an instance of {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime }
     * 
     */
    public Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime createZahtevInformacijeOTraziocuTraziocFizickoLiceIme() {
        return new Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Ime();
    }

    /**
     * Create an instance of {@link Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime }
     * 
     */
    public Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime createZahtevInformacijeOTraziocuTraziocFizickoLicePrezime() {
        return new Zahtev.InformacijeOTraziocu.Trazioc.FizickoLice.Prezime();
    }

    /**
     * Create an instance of {@link Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta }
     * 
     */
    public Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta createZahtevElementiZahtevaDostavljanjeKopijeDokumenta() {
        return new Zahtev.ElementiZahteva.DostavljanjeKopijeDokumenta();
    }

    /**
     * Create an instance of {@link Zahtev.PodaciOOrganu.NazivOrgana }
     * 
     */
    public Zahtev.PodaciOOrganu.NazivOrgana createZahtevPodaciOOrganuNazivOrgana() {
        return new Zahtev.PodaciOOrganu.NazivOrgana();
    }

    /**
     * Create an instance of {@link Zahtev.PodaciOOrganu.SedisteOrgana }
     * 
     */
    public Zahtev.PodaciOOrganu.SedisteOrgana createZahtevPodaciOOrganuSedisteOrgana() {
        return new Zahtev.PodaciOOrganu.SedisteOrgana();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Zahtev.PodaciOOrganu }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Zahtev.PodaciOOrganu }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "podaci_o_organu", scope = Zahtev.class)
    public JAXBElement<Zahtev.PodaciOOrganu> createZahtevPodaciOOrganu(Zahtev.PodaciOOrganu value) {
        return new JAXBElement<Zahtev.PodaciOOrganu>(_ZahtevPodaciOOrganu_QNAME, Zahtev.PodaciOOrganu.class, Zahtev.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Zahtev.ZahtevNaOsnovu }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Zahtev.ZahtevNaOsnovu }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "zahtev_na_osnovu", scope = Zahtev.class)
    public JAXBElement<Zahtev.ZahtevNaOsnovu> createZahtevZahtevNaOsnovu(Zahtev.ZahtevNaOsnovu value) {
        return new JAXBElement<Zahtev.ZahtevNaOsnovu>(_ZahtevZahtevNaOsnovu_QNAME, Zahtev.ZahtevNaOsnovu.class, Zahtev.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Zahtev.MestoIDatum }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Zahtev.MestoIDatum }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "mesto_i_datum", scope = Zahtev.class)
    public JAXBElement<Zahtev.MestoIDatum> createZahtevMestoIDatum(Zahtev.MestoIDatum value) {
        return new JAXBElement<Zahtev.MestoIDatum>(_ZahtevMestoIDatum_QNAME, Zahtev.MestoIDatum.class, Zahtev.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Zahtev.ElementiZahteva }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Zahtev.ElementiZahteva }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "elementiZahteva", scope = Zahtev.class)
    public JAXBElement<Zahtev.ElementiZahteva> createZahtevElementiZahteva(Zahtev.ElementiZahteva value) {
        return new JAXBElement<Zahtev.ElementiZahteva>(_ZahtevElementiZahteva_QNAME, Zahtev.ElementiZahteva.class, Zahtev.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "informacije_na_koje_se_odnosi_zahtev", scope = Zahtev.class)
    public JAXBElement<String> createZahtevInformacijeNaKojeSeOdnosiZahtev(String value) {
        return new JAXBElement<String>(_ZahtevInformacijeNaKojeSeOdnosiZahtev_QNAME, String.class, Zahtev.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Zahtev.InformacijeOTraziocu }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Zahtev.InformacijeOTraziocu }{@code >}
     */
    @XmlElementDecl(namespace = "", name = "informacije_o_traziocu", scope = Zahtev.class)
    public JAXBElement<Zahtev.InformacijeOTraziocu> createZahtevInformacijeOTraziocu(Zahtev.InformacijeOTraziocu value) {
        return new JAXBElement<Zahtev.InformacijeOTraziocu>(_ZahtevInformacijeOTraziocu_QNAME, Zahtev.InformacijeOTraziocu.class, Zahtev.class, value);
    }

}
