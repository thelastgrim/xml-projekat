package com.team.xml.controllers;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;
import org.apache.jena.rdf.model.Property;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XUpdateQueryService;

import com.team.xml.templates.ZahtevCirUpdate;
import com.team.xml.util.AuthenticationUtil;
import com.team.xml.util.AuthenticationUtilRDF;
import com.team.xml.util.ControllerInterface;
import com.team.xml.util.SparqlUtil;
import com.team.xml.util.AuthenticationUtilRDF.ConnectionProperties;

import static com.team.xml.util.Constants.*;

@Controller
@RequestMapping("/zahtevcir")
public class ZahtjevController implements ControllerInterface {

	private AuthenticationUtil.ConnectionProperties conn;
	
	@Override
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<HttpStatus> insert()
			throws IOException, ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
		conn = AuthenticationUtil.loadProperties();

        Class<?> cl = Class.forName(conn.driver);
        
        Database database = (Database) cl.newInstance();
        database.setProperty("create-database", "true");
        
        DatabaseManager.registerDatabase(database);
        
        Collection col = null;
        String xmlData = 
        		"<zahtev\r\n" + 
        		"    \r\n" + 
        		"    xmlns = \"http://localhost:8080/zahtevcir\"\r\n" + 
        		"    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\r\n" + 
        		"    xmlns:pred=\"http://www.xml.com/predicate/\"\r\n" + 
        		"    xsi:schemaLocation=\"http://zahtevcir zahtevcir.xsd\"\r\n" + 
        		"    mesto=\"Derventa\" \r\n" + 
        		"    datum=\"2021-01-01\"\r\n" + 
        		"    id=\"ID_1\">\r\n" + 
        		"    <podaci_o_organu>\r\n" + 
        		"        <naziv_organa property = \"pred:naziv_organa\">Opstina Derventa</naziv_organa>\r\n" + 
        		"        <sediste_organa property = \"pred:sediste_organa\">BANJALUKA</sediste_organa>\r\n" + 
        		"    </podaci_o_organu>\r\n" + 
        		"    <zahtev_na_osnovu>\r\n" + 
        		"        <broj_clana>15</broj_clana>\r\n" + 
        		"        <stav>1</stav>\r\n" + 
        		"        <zakon> Закона о слободном приступу информацијама од јавног значаја („Службени гласник РС“, бр. 120/04, 54/07, 104/09 и 36/10)</zakon>\r\n" + 
        		"    </zahtev_na_osnovu>\r\n" + 
        		"    <mesto_i_datum>\r\n" + 
        		"        <mesto>Derventa</mesto>\r\n" + 
        		"        <dan>12</dan>\r\n" + 
        		"        <mesec>11</mesec>\r\n" + 
        		"        <godina>1995</godina>\r\n" + 
        		"    </mesto_i_datum>\r\n" + 
        		"    <elementiZahteva>\r\n" + 
        		"        <uvid_u_dokument>true</uvid_u_dokument>\r\n" + 
        		"        <posedovanje_informacije>true</posedovanje_informacije>\r\n" + 
        		"        <kopija_dokumenta>true</kopija_dokumenta>\r\n" + 
        		"        <dostavljanje_kopije_dokumenta>\r\n" + 
        		"            <postom>false</postom>\r\n" + 
        		"            <elektronskom_postom>false</elektronskom_postom>\r\n" + 
        		"            <faksom>false</faksom>\r\n" + 
        		"            <drugi_nacin>\r\n" + 
        		"                neki drugi nacin\r\n" + 
        		"            </drugi_nacin>\r\n" + 
        		"        </dostavljanje_kopije_dokumenta>\r\n" + 
        		"    </elementiZahteva>\r\n" + 
        		"    <informacije_na_koje_se_odnosi_zahtev>nacrt budyet opstine</informacije_na_koje_se_odnosi_zahtev>\r\n" + 
        		"    <informacije_o_traziocu>\r\n" + 
        		"        <trazioc>\r\n" + 
        		"            <fizicko_lice>\r\n" + 
        		"                <ime property = \"pred:trazioc_ime\" >Snjezana</ime>\r\n" + 
        		"                <prezime property = \"pred:trazioc_prezime\">Simic</prezime>\r\n" + 
        		"            </fizicko_lice>\r\n" + 
        		"            <pravno_lice/>\r\n" + 
        		"        </trazioc>\r\n" + 
        		"        <adresa>\r\n" + 
        		"            <ulica>Hajduk veljka</ulica>\r\n" + 
        		"            <broj>179</broj>\r\n" + 
        		"            <grad>24400</grad>\r\n" + 
        		"        </adresa>\r\n" + 
        		"        <drugi_podaci_za_kontakt>654835486</drugi_podaci_za_kontakt>\r\n" + 
        		"    </informacije_o_traziocu>\r\n" + 
        		"</zahtev>\r\n";
        try {
        	 col = DatabaseManager.getCollection(conn.uri + COLLECTION_URI, conn.user, conn.password);
             col.setProperty("indent", "yes");
         	
             XUpdateQueryService xupdateService = (XUpdateQueryService) col.getService("XUpdateQueryService", "1.0");
             xupdateService.setProperty("indent", "yes");
             xupdateService.updateResource(ZAHTEVCIR_ID, String.format(ZahtevCirUpdate.APPEND, OBAVESTENJE_SPARQL_NAMED_GRAPH_URI, xmlData));
             
        }
finally {
        	
            if(col != null) {
                try { 
                	col.close();
                } catch (XMLDBException xe) {
                	xe.printStackTrace();
                }
            }
        }
        //rdf
        ConnectionProperties conn = AuthenticationUtilRDF.loadProperties();
		
		Model model = ModelFactory.createDefaultModel();
		model.setNsPrefix("pred", PREDICATE_NAMESPACE);

		// Making the changes manually 
		Resource resource = model.createResource("http://localhost:8080/rdf/examples/zahtevcir/ID_1");
		
		Property property1 = model.createProperty(PREDICATE_NAMESPACE, "zahtev_za");
		Literal literal1 = model.createLiteral("dokument");
		
		Property property2 = model.createProperty(PREDICATE_NAMESPACE, "mesto");
		Literal literal2 = model.createLiteral("Derventa");
		
		Property property3 = model.createProperty(PREDICATE_NAMESPACE, "datum");
		Literal literal3 = model.createLiteral("2020-05-05");
		
		Property property4 = model.createProperty(PREDICATE_NAMESPACE, "naziv_organa");
		Literal literal4 = model.createLiteral("Visi sud");
		
		Property property5 = model.createProperty(PREDICATE_NAMESPACE, "sediste");
		Literal literal5 = model.createLiteral("Derventi");
		
		Property property6 = model.createProperty(PREDICATE_NAMESPACE, "podnosilac_ime");
		Literal literal6 = model.createLiteral("Pera");
		
		Property property7 = model.createProperty(PREDICATE_NAMESPACE, "podnosilac_prezime");
		Literal literal7 = model.createLiteral("Peric");
				
		
		// Adding the statements to the model
				Statement statement1 = model.createStatement(resource, property1, literal1);
				Statement statement2 = model.createStatement(resource, property2, literal2);
				Statement statement3 = model.createStatement(resource, property3, literal3);
				Statement statement4 = model.createStatement(resource, property4, literal4);
				Statement statement5 = model.createStatement(resource, property5, literal5);
				Statement statement6 = model.createStatement(resource, property6, literal6);
				Statement statement7 = model.createStatement(resource, property7, literal7);
	
				model.add(statement1);
				model.add(statement2);
				model.add(statement3);
				model.add(statement4);
				model.add(statement5);
				model.add(statement6);
				model.add(statement7);
				
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				model.write(out, SparqlUtil.NTRIPLES);
				
				String sparqlUpdate = SparqlUtil.insertData(conn.dataEndpoint + ZAHTEVCIR_SPARQL_NAMED_GRAPH_URI, new String(out.toByteArray()));
				
				UpdateRequest update = UpdateFactory.create(sparqlUpdate);
			
			    UpdateProcessor processor = UpdateExecutionFactory.createRemote(update, conn.updateEndpoint);
				processor.execute();
			
				model.close();
				
				System.out.println("[INFO] Insertion done");
			    
			    return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
